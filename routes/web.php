<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/data-urutan', [App\Http\Controllers\HomeController::class, 'urutanOrder'])->name('home.urutanOrder');
Route::group(['middleware' => ['auth']], function() {

    // ADMINISTRATOR ONLY
    
    Route::prefix('/permission')->middleware(['permission:menu-permission'])->group(function(){
        Route::get('', [App\Http\Controllers\PermissionController::class, 'index'])->name('permission.index');
        Route::get('/get-data', [App\Http\Controllers\PermissionController::class, 'getData'])->name('permission.getData');
        Route::post('/save-data', [App\Http\Controllers\PermissionController::class, 'saveData'])->name('permission.saveData');
        Route::get('/edit-data/{id}', [App\Http\Controllers\PermissionController::class, 'editData'])->name('permission.editData');
        Route::post('/update-data', [App\Http\Controllers\PermissionController::class, 'updateData'])->name('permission.updateData');
        Route::get('/delete-permission/{id}', [App\Http\Controllers\PermissionController::class, 'deleteData'])->name('permission.deleteData');
    });
    Route::prefix('/role')->middleware(['permission:menu-role'])->group(function(){
        Route::get('/', [App\Http\Controllers\RoleController::class, 'index'])->name('role.index');
        Route::get('/get-data', [App\Http\Controllers\RoleController::class, 'getData'])->name('role.getData');
        Route::post('/save-data', [App\Http\Controllers\RoleController::class, 'saveData'])->name('role.saveData');
        Route::get('/edit-data/{id}', [App\Http\Controllers\RoleController::class, 'editData'])->name('role.editData');
        Route::post('/update-data', [App\Http\Controllers\RoleController::class, 'updateData'])->name('role.updateData');
        Route::post('/save-permission-role/permission', [App\Http\Controllers\RoleController::class, 'storePermission'])->name('role.storePermission');
        Route::get('/delete-role/{id}', [App\Http\Controllers\RoleController::class, 'destroy'])->name('role.destroy');
    });

    Route::prefix('/register')->middleware(['permission:menu-user'])->group(function(){
        Route::get('', [App\Http\Controllers\RegisterController::class, 'index'])->name('register.index');
        Route::get('/data', [App\Http\Controllers\RegisterController::class, 'data'])->name('register.data');
        Route::get('/edit/{id}', [App\Http\Controllers\RegisterController::class, 'edit'])->name('register.edit');
        Route::post('/store', [App\Http\Controllers\RegisterController::class, 'store'])->name('register.store');
        Route::post('/update/{id}', [App\Http\Controllers\RegisterController::class, 'update'])->name('register.update');
        Route::delete('/delete/{id}', [App\Http\Controllers\RegisterController::class, 'delete'])->name('register.delete');
        Route::post('/resetPass/{id}', [App\Http\Controllers\RegisterController::class, 'resetPassword'])->name('register.resetPassword');
    });

    Route::prefix('/master')->middleware(['permission:master-barang|master-process|master-product|master-size|master-supplier|master-bahan-baku'])->group(function(){
        Route::get('/barang', [App\Http\Controllers\Master\BarangController::class, 'index'])->name('barang.index');
        Route::get('/data-barang', [App\Http\Controllers\Master\BarangController::class, 'dataBarang'])->name('barang.dataBarang');
        Route::get('/edit-barang/{id}', [App\Http\Controllers\Master\BarangController::class, 'editBarang'])->name('barang.editBarang');
        Route::get('/delete-barang/{id}', [App\Http\Controllers\Master\BarangController::class, 'deleteBarang'])->name('barang.deleteBarang');
        Route::post('/add-barang', [App\Http\Controllers\Master\BarangController::class, 'addBarang'])->name('barang.addBarang');
        Route::post('/update-barang', [App\Http\Controllers\Master\BarangController::class, 'updateBarang'])->name('barang.updateBarang');
        
        Route::prefix('/process')->group(function(){
            Route::get('', [App\Http\Controllers\Master\ProcessController::class, 'index'])->name('process.index');
            Route::post('/store', [App\Http\Controllers\Master\ProcessController::class, 'store'])->name('process.store');
            Route::get('/data', [App\Http\Controllers\Master\ProcessController::class, 'data_process'])->name('process.data');
            Route::post('/updateData', [App\Http\Controllers\Master\ProcessController::class, 'update_data'])->name('process.update_data');
            Route::post('/deleteData', [App\Http\Controllers\Master\ProcessController::class, 'delete_data'])->name('process.delete_data');
        });
        
        // PRODUCT
        Route::get('/product', [App\Http\Controllers\Master\ProductController::class, 'index'])->name('product.index');
        Route::get('/data-product', [App\Http\Controllers\Master\ProductController::class, 'dataProduct'])->name('product.dataProduct');
        Route::get('/edit-product/{id}', [App\Http\Controllers\Master\ProductController::class, 'editProduct'])->name('product.editProduct');
        Route::get('/delete-product/{id}', [App\Http\Controllers\Master\ProductController::class, 'deleteProduct'])->name('product.deleteProduct');
        Route::post('/add-product', [App\Http\Controllers\Master\ProductController::class, 'addProduct'])->name('product.addProduct');
        Route::post('/update-product', [App\Http\Controllers\Master\ProductController::class, 'updateProduct'])->name('product.updateProduct');

        // SIZE
        Route::get('/size', [App\Http\Controllers\Master\SizeController::class, 'index'])->name('size.index');
        Route::get('/data-size', [App\Http\Controllers\Master\SizeController::class, 'dataSize'])->name('size.dataSize');
        Route::get('/edit-size/{id}', [App\Http\Controllers\Master\SizeController::class, 'editSize'])->name('size.editSize');
        Route::get('/delete-size/{id}', [App\Http\Controllers\Master\SizeController::class, 'deleteSize'])->name('size.deleteSize');
        Route::post('/add-size', [App\Http\Controllers\Master\SizeController::class, 'addSize'])->name('size.addSize');
        Route::post('/update-size', [App\Http\Controllers\Master\SizeController::class, 'updateSize'])->name('size.updateSize');     
        // BAHAN BAKU
        Route::get('/bahan-baku', [App\Http\Controllers\Master\BahanBakuController::class, 'index'])->name('bahan_baku.index');
        Route::get('/data-bahan-baku', [App\Http\Controllers\Master\BahanBakuController::class, 'dataBahanBaku'])->name('bahan_baku.dataBahanBaku');
        Route::get('/edit-bahan-baku/{id}', [App\Http\Controllers\Master\BahanBakuController::class, 'editBahanBaku'])->name('bahan_baku.editBahanBaku');
        Route::get('/delete-bahan-baku/{id}', [App\Http\Controllers\Master\BahanBakuController::class, 'deleteBahanBaku'])->name('bahan_baku.deleteBahanBaku');
        Route::post('/add-bahan-baku', [App\Http\Controllers\Master\BahanBakuController::class, 'addBahanBaku'])->name('bahan_baku.addBahanBaku');
        Route::post('/update-bahan-baku', [App\Http\Controllers\Master\BahanBakuController::class, 'updateBahanBaku'])->name('bahan_baku.updateBahanBaku');     

        // SUPPLIER
        Route::get('/supplier', [App\Http\Controllers\Master\SupplierController::class, 'index'])->name('supplier.index');
        Route::get('/data-supplier', [App\Http\Controllers\Master\SupplierController::class, 'dataSupplier'])->name('supplier.dataSupplier');
        Route::get('/edit-supplier/{id}', [App\Http\Controllers\Master\SupplierController::class, 'editSupplier'])->name('supplier.editSupplier');
        Route::get('/delete-supplier/{id}', [App\Http\Controllers\Master\SupplierController::class, 'deleteSupplier'])->name('supplier.deleteSupplier');
        Route::post('/add-supplier', [App\Http\Controllers\Master\SupplierController::class, 'addSupplier'])->name('supplier.addSupplier');
        Route::post('/update-supplier', [App\Http\Controllers\Master\SupplierController::class, 'updateSupplier'])->name('supplier.updateSupplier');
    });

    Route::prefix('/production')->middleware(['permission:transaction-production'])->group(function(){
        Route::get('/index', [App\Http\Controllers\Transaction\ProductionController::class, 'index'])->name('production.index');
        Route::get('/data-production', [App\Http\Controllers\Transaction\ProductionController::class, 'getDataProgress'])->name('production.getDataProgress');
        Route::get('/confirm-production', [App\Http\Controllers\Transaction\ProductionController::class, 'confirmOrder'])->name('production.confirmOrder');
        Route::get('/detail-order', [App\Http\Controllers\Transaction\ProductionController::class, 'detailOrder'])->name('production.detailOrder');
        Route::get('/process-production', [App\Http\Controllers\Transaction\ProductionController::class, 'processProduction'])->name('production.processProduction');
        Route::get('/finished-production', [App\Http\Controllers\Transaction\ProductionController::class, 'productionFinished'])->name('production.productionFinished');
    });

    Route::prefix('/transaction')->middleware(['permission:transaction-invoice|transaction-stock|transaction-order'])->group(function(){
        Route::get('/order', [App\Http\Controllers\Transaction\OrderController::class, 'index'])->name('order.index');
        Route::get('/data-order', [App\Http\Controllers\Transaction\OrderController::class, 'dataOrder'])->name('order.dataOrder');

        Route::post('/add-order', [App\Http\Controllers\Transaction\OrderController::class, 'addOrder'])->name('order.addOrder');
        Route::post('/total-price', [App\Http\Controllers\Transaction\OrderController::class, 'totalPrice'])->name('order.totalPrice');
        Route::post('/total-based-disc', [App\Http\Controllers\Transaction\OrderController::class, 'totalBasedDisc'])->name('order.totalBasedDisc');
        Route::post('/update-order', [App\Http\Controllers\Transaction\OrderController::class, 'updateOrder'])->name('order.updateOrder');
        Route::post('/gen-invoice', [App\Http\Controllers\Transaction\OrderController::class, 'genInvoice'])->name('order.genInvoice');
        
        Route::get('/invoice', [App\Http\Controllers\Transaction\InvoiceController::class, 'index'])->name('invoice.index');
        Route::get('/data-invoice', [App\Http\Controllers\Transaction\InvoiceController::class, 'dataInvoice'])->name('invoice.dataInvoice');
        Route::get('/delete-invoice/{id}', [App\Http\Controllers\Transaction\InvoiceController::class, 'deleteInvoice'])->name('invoice.deleteInvoice');
        Route::get('/print-invoice/{id}', [App\Http\Controllers\Transaction\InvoiceController::class, 'printInvoice'])->name('invoice.printInvoice');
        Route::get('/cancel-order', [App\Http\Controllers\Transaction\InvoiceController::class, 'cancelOrder'])->name('invoice.cancelOrder');
        
        Route::get('/stock', [App\Http\Controllers\Transaction\StockController::class, 'index'])->name('stock.index');
        Route::post('/add-stock', [App\Http\Controllers\Transaction\StockController::class, 'addStock'])->name('stock.addStock');
        Route::get('/data-stock', [App\Http\Controllers\Transaction\StockController::class, 'dataStock'])->name('stock.dataStock');
        Route::get('/edit-stock/{id}', [App\Http\Controllers\Transaction\StockController::class, 'editStock'])->name('stock.editStock');
        Route::post('/update-stock', [App\Http\Controllers\Transaction\StockController::class, 'updateStock'])->name('stock.updateStock');
    });

    Route::prefix('/reports')->middleware(['permission:laporan-transaki|laporan-invoice|laporan-stock'])->group(function(){
        Route::get('/transkaksi', [App\Http\Controllers\Report\ReportsController::class, 'transaksi'])->name('report.transaksi');
        Route::get('/download-transkaksi', [App\Http\Controllers\Report\ReportsController::class, 'downloadTransaksi'])->name('report.downloadTransaksi');
        Route::get('/tagihan', [App\Http\Controllers\Report\ReportsController::class, 'tagihan'])->name('report.tagihan');
        Route::get('/download-tagihan', [App\Http\Controllers\Report\ReportsController::class, 'downloadTagihan'])->name('report.downloadTagihan');
        Route::get('/stock', [App\Http\Controllers\Report\ReportsController::class, 'stock'])->name('report.stock');
        Route::get('/download-stock', [App\Http\Controllers\Report\ReportsController::class, 'downloadStockBahan'])->name('report.downloadStockBahan');
    });

});
