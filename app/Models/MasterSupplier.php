<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterSupplier extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'master_supplier';
    protected $fillable = ['description','created_by','updated_by','deleted_by','deleted_at','price','created_at','updated_at'];
    protected $dates = ['created_at','updated_at'];

    public function pemasok()
    {
        return $this->hasMany(StockBahan::class, 'pemasok');
    }
}
