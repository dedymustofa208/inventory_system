<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionOrderDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'transaction_order_details';
    protected $fillable = [
        'order_id', 'qty', 'unit_price', 'created_by', 'updated_by', 'product_id'
    ];
    protected $dates = ['created_at','updated_at'];

    public function getProduct()
    {
        return $this->belongsTo(MasterProduct::class, 'id', 'product_id');
    }
}
