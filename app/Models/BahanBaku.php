<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BahanBaku extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'master_bahan_baku';
    protected $fillable = ['description','color_name','code_bahan','bahan_type', 'unit_prices', 'uom' ,'created_by','updated_by','deleted_by','deleted_at','created_at','updated_at'];
    protected $dates = ['created_at','updated_at'];

    public function bahan()
    {
        return $this->hasMany(StockBahan::class, 'id');
    }
}
