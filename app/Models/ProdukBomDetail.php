<?php

namespace App\Models;

use App\Models\MasterProduct;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProdukBomDetail extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];
	protected $table = 'produk_bom_details';
    protected $dates = ['created_at', 'updated_at'];
	protected $fillable = [
        'product_id', 'material', 'uom', 'cons',
        'created_by','updated_by','deleted_by','deleted_at'
    ];

    public function getHeader()
    {
        return $this->belongsTo(MasterProduct::class, 'id','product_id');
    }
}
