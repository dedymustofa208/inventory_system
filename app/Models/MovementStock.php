<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MovementStock extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'movement_stock';
    protected $fillable = ['stock_id','invoice_number','created_by','qty','uom','description'];
    protected $dates = ['created_at','updated_at'];

}
