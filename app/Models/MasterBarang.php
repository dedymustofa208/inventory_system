<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MasterBarang extends Model
{
    use SoftDeletes;
    
    protected $primaryKey = 'id';
    protected $table = 'master_barang';
    protected $fillable = ['description','created_by','updated_by','deleted_by','deleted_at','price','created_at','updated_at'];
    protected $dates = ['created_at','updated_at'];

    public function masterProducts()
    {
        return $this->hasMany(MasterProduct::class, 'barang_id');
    }
}
