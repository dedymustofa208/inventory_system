<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterProduct extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'master_product';
    protected $fillable = ['barang_id','process_id','size','total_price','created_by','updated_by','deleted_by','deleted_at','photo_product','created_at','updated_at'];
    protected $dates = ['created_at','updated_at'];

    public function barang()
    {
        return $this->belongsTo(MasterBarang::class, 'barang_id', 'id');
    }

    public function process()
    {
        return $this->belongsTo(MasterProcess::class, 'process_id', 'id');
    }
}
