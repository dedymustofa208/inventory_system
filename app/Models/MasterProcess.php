<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SoftDelete;

class MasterProcess extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'master_process';
    protected $fillable = ['description','created_by','updated_by','deleted_by','deleted_at','price'];
    protected $dates = ['created_at','updated_at'];

    public function masterProducts()
    {
        return $this->hasMany(MasterProduct::class, 'process_id');
    }
}
