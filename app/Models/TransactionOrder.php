<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionOrder extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'transaction_order';
    protected $fillable = ['client_order','no_telp','alamat','product_id','qty','pajak','diskon','bonus','total_price','metode_bayar','is_completed','remark','created_by','updated_by','deleted_by','deleted_at','created_at','updated_at','invoice_number','status'];
    protected $dates = ['created_at','updated_at'];

    public function getDetails()
    {
        return $this->hasMany(TransactionOrderDetail::class, 'order_id', 'id');
    }
}
