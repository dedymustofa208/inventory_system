<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionProduction extends Model
{
    protected $guarded = ['id'];
	protected $table = 'transaction_production';
    protected $dates = ['created_at', 'updated_at'];
	protected $fillable = ['invoice_number','stock_id','qty','uom','status','is_done','created_by','updated_by','deleted_by','deleted_at','updated_at','created_at'];
}
