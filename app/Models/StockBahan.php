<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockBahan extends Model
{
    protected $guarded = ['id'];
	protected $table = 'transaction_stock_bahan';
    protected $dates = ['created_at', 'updated_at'];
	protected $fillable = ['bahan_id','qty','qty_used','qty_available','price','pemasok','created_by','updated_by','deleted_by','deleted_at','uom'];

    public function namaBahan()
    {
        return $this->belongsTo(BahanBaku::class, 'bahan_id', 'id');
    }

    public function supplierName()
    {
        return $this->belongsTo(MasterSupplier::class, 'id');
    }
    
}
