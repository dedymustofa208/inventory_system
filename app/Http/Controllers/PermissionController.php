<?php

namespace App\Http\Controllers;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use StdClass;
use Str;
use DataTables;
use Carbon\Carbon;

use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index(){
        return view('admin.permission.index');
    }

    public function getData(Request $request){
        $data = Permission::orderBy('updated_at','DESC')->get();
        return datatables()->of($data)
        ->addColumn('action',function($data){
            return '<div class="row">
            <button type="button" data-id="'.$data->id.'" class="btn btn-info edit mx-sm-1"><a><i class="fas fa-edit"></i> Edit</a></button>
            <button type="button" data-id="'.$data->id.'" class="btn btn-danger deletes"><a><i class="fas fa-trash"></i> Delete</a></button>
            </div>';
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function saveData(Request $request)
    {
        $this->validate($request, [
            'permission_name' => 'required|min:3'
        ]);

        if(Permission::where('name',Str::slug(strtolower($request->permission_name)))->exists()){
            return response()->json('Permission Name Already Exists!', 422);
        }

        $permission = Permission::insertGetId([
            'name'          => Str::slug(trim($request->permission_name)),
            'guard_name'    => 'web',
            'created_at'    => Carbon::now()
        ]);
        return response()->json('Permission Saved', 200);
    }

    public function editData($id)
    {
        $permission         = Permission::find($id);
        if(count(explode('-',$permission->name)) > 1){
            $name = str_replace('-',' ',$permission->name);
        }else{
            $name = $permission->name;
        }
        $obj                = new StdClass();
        $obj->id            = $id;
        $obj->name          = $name;

		return response()->json($obj,200);
    }

    public function deleteData($id)
    {
        $check = DB::table('role_has_permissions')->where('permission_id',$id)->first();
        if($check != null){
            return response()->json('This Permission Can\'t be Deleted!',422);
        }
        Permission::findorFail($id)->delete();
        return response()->json('Permission Delete',200);
    }

    public function updateData(Request $request)
    {
        $this->validate($request, [
            'id_update'             => 'required',
            'permission_name_update' => 'required|min:3'
        ]);

        $id = $request->id_update;

        if(Permission::where('name',Str::slug($request->permission_name_update))->where('id','!=',$id)->exists()){
            return response()->json('Permission sudah ada!', 422);
        }            

        $permission = Permission::find($id);
        $permission->name = Str::slug($request->permission_name_update);
        $permission->save();

        return response()->json('success', 200);
    }
}
