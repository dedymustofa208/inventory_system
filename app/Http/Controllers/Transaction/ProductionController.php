<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\TransactionOrder;
use App\Models\StockBahan;
use App\Models\MovementStock;
use App\Models\TransactionProduction;
use DataTables;
use Auth;
use Config;
use Carbon;
use StdClass;

class ProductionController extends Controller
{
    public function index(){

        return view('transaction.production.index');
    }

    public function getDataProgress(Request $request)
    {
        if($request->ajax()){
            $data = TransactionOrder::whereNull('deleted_at')->where('status','<>','finished')->orderBy('created_at');
            return datatables()->of($data)
            ->editColumn('client_order',function($data){
                return strtoupper($data->client_order);
            })
            ->editColumn('status',function($data){
                return strtoupper($data->status);
            })
            ->editColumn('created_at',function($data){
                return Carbon::parse($data->created_at)->format('d M Y H:i:s');
            })
            ->addColumn('action',function($data){
                if($data->status == 'draft'){
                    return '<div class="btn-group text-center">
                        <button type="button" data-invoice_no="'.$data->invoice_number.'" class="btn btn-info proses mr-3"><a><i class="fas fa-paint-roller"></i> Proses</a></button>
                    </div>';
                }else{
                    return '<div class="btn-group text-center">
                        <button type="button" data-invoice_no="'.$data->invoice_number. '" class="btn btn-success selesai"><i class="fas fa-handshake"></i> Selesai</button>
                    </div>';
                }
            })
            ->rawColumns(['action'])
            ->make(true);
        }else{
            $data = [];
            return datatables()->of($data)
            ->make(true);
        }
    }

    public function detailOrder(Request $request)
    {
        if($request->ajax()){
            $invoice = $request->invoice;
            $data = DB::table('v_order_detail_rev')->where('status','draft')->where('invoice_number',$invoice);
            return datatables()->of($data)
            ->editColumn('client_order',function($data){
                return strtoupper($data->client_order);
            })
            ->editColumn('status',function($data){
                return strtoupper($data->status);
            })
            ->editColumn('nama_barang',function($data){
                return strtoupper($data->nama_barang);
            })
            ->editColumn('size',function($data){
                return strtoupper($data->size);
            })
            ->editColumn('process_name',function($data){
                return strtoupper($data->process_name);
            })
            ->editColumn('order_date',function($data){
                return Carbon::parse($data->order_date)->format('d M Y H:i:s');
            })
            ->rawColumns(['action'])
            ->make(true);
        }else{
            $data = [];
            return datatables()->of($data)
            ->make(true);
        }
    }

    public function confirmOrder(Request $request)
    {
        $invoice = $request->invoice;
        $cek = TransactionOrder::whereNull('deleted_at')->where('status','draft')->orderBy('created_at','ASC')->first();
        if($cek->invoice_number != $invoice){
            return response()->json('Anda Harus Menyelesaikan Order '.$cek->invoice_number.' Dahulu',422);
        }else{
            $obj = new StdClass;
            $obj->invoice = $cek->invoice_number;
            $obj->client_order = $cek->client_order;
            return response()->json($obj,200);
        }
    }
    
    public function processProduction(Request $request)
    {
        try {
            DB::beginTransaction();
            $invoice = $request->invoice;
            $detailOrder = DB::table('v_order_detail_rev')->where('invoice_number', $invoice)->get();
            foreach ($detailOrder as $detailKey => $detailVal) {
                $qtyOrder = $detailVal->qty;
                $cekCons = DB::table('master_product as mp')
                            ->selectRaw("mb.description as barang, pbd.material, pbd.uom, pbd.cons, mpr.description as process, mp.size")
                            ->join('produk_bom_details as pbd', 'pbd.product_id', 'mp.id')
                            ->join('master_process as mpr', 'mpr.id', 'mp.process_id')
                            ->join('master_barang as mb', 'mb.id', 'mp.barang_id')
                            ->join('transaction_order_details as tod', 'tod.product_id', 'mp.id')
                            ->join('transaction_order as to', 'to.id', 'tod.order_id')
                            ->whereNull('mp.deleted_at')
                            ->where('mp.id', $detailVal->product_id)
                            ->where('mpr.description', $detailVal->process_name)
                            ->get();

                if ($cekCons->isEmpty()) {
                    return response()->json('Terjadi Kesalahan / Produk Sudah Dihapus', 404);
                }

                foreach ($cekCons as $cekConsKey => $cekConsVal) {
                    $material = $cekConsVal->material;
                    $cons = $cekConsVal->cons;
                    $uom = $cekConsVal->uom;
                    $consOrder = $cons * $qtyOrder;

                    $cekStocks = DB::table('master_bahan_baku as mbb')
                        ->selectRaw("tsb.id as stock_id, tsb.qty_available, tsb.uom, mbb.description")
                        ->join('transaction_stock_bahan as tsb', 'tsb.bahan_id', 'mbb.id')
                        ->where('mbb.bahan_type', $material)
                        ->where('tsb.qty_available', '>', '0')
                        ->whereNull('mbb.deleted_at')
                        ->orderBy('tsb.created_at')
                        ->sharedLock()
                        ->get();

                    if ($cekStocks->isEmpty()) {
                        return response()->json('Bahan Baku Tidak Ditemukan / Habis, Material: ' . $material . ' | Need: ' . $consOrder, 404);
                    }

                    $conversionRates = Config::get('constants.conversion');
                    $limitStock = Config::get('constants.limit_stock');
                    $foundStock = false;

                    foreach ($cekStocks as $cekStock) {
                        if ($consOrder <= 0) break;

                        $stock_id = $cekStock->stock_id;
                        $availableStock = (int)$cekStock->qty_available * $conversionRates[$material];

                        if ($availableStock >= $limitStock[$material]) {
                            $maxUsableStock = $availableStock - $limitStock[$material];
                            $qty_used = min($maxUsableStock, $consOrder);
                            $qty_available = ($availableStock - $qty_used) / $conversionRates[$material];

                            $consOrder -= $qty_used;
                            $foundStock = true;

                            if ($qty_used > 0) {
                                TransactionProduction::insert([
                                    'invoice_number' => $invoice,
                                    'stock_id' => $stock_id,
                                    'qty' => $qty_used,
                                    'uom' => $uom,
                                    'status' => 'onprogress',
                                    'is_done' => false,
                                    'created_by' => Auth::user()->id,
                                    'created_at' => Carbon::now(),
                                    'updated_at' => null
                                ]);
    
                                MovementStock::create([
                                    'stock_id' => $stock_id,
                                    'invoice_number' => $invoice,
                                    'qty' => $qty_used,
                                    'uom' => $uom,
                                    'description' => 'production',
                                    'created_by' => Auth::user()->id,
                                    'created_at' => Carbon::now()
                                ]);
    
                                StockBahan::where('id', $stock_id)->whereNull('deleted_at')->update([
                                    'qty_used' => DB::raw("qty_used + ($qty_used / $conversionRates[$material])"),
                                    'qty_available' => $qty_available,
                                    'updated_at' => now(),
                                    'updated_by' => Auth::user()->id
                                ]);
                            }
                        } else {
                            continue;
                        }
                    }
                    if (!$foundStock) {
                        return response()->json('Tidak ada stok tersisa yang memenuhi kebutuhan', 404);
                    }
                }
            }
            TransactionOrder::where('invoice_number', $invoice)->update([
                'status' => 'onprocess',
                'updated_at' => Carbon::now(),
                'updated_by' => Auth::user()->id
            ]);          
            DB::commit();
            return response()->json(200);
        }catch (\Exception $e){
            DB::rollback();
            $message = $e->getMessage();
            return response()->json($message, 500);
        }
    }

    public function productionFinished(Request $request)
    {
        $invoice = $request->invoice;
        try {
            DB::beginTransaction();
                $cek_pending = TransactionOrder::where('status','onprocess')->orderBy('created_at')->first();
                if($cek_pending != null && $cek_pending->invoice_number != $invoice){
                    return response()->json('Anda Harus Menyelesaikan Invoice:'.$cek_pending->invoice_number.' Dahulu!',422);
                }
                TransactionOrder::where('invoice_number',$invoice)->update([
                    'is_completed'      => true,
                    'status'            => 'finished'
                ]);
                TransactionProduction::where('invoice_number',$invoice)->update([
                    'status'        => 'finished',
                    'updated_at'    => Carbon::now(),
                    'updated_by'    => Auth::user()->id
                ]);
            DB::commit();
            return response()->json(200);
        }catch (\Exception $e){
            DB::rollback();
            $message = $e->getMessage();
            return response()->json($message, 500);
        }
    }
}
