<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterProduct;
use App\Models\MasterBarang;
use App\Models\TransactionOrder;
use App\Models\TransactionProduction;
use Auth;
use Carbon;
use DB;
use DataTables;
use Config;
use PDF;

class InvoiceController extends Controller
{
    public function index(){
        return view('transaction.invoice.index');
    }

    public function dataInvoice(Request $request){
        if($request->ajax()){
            $data = TransactionOrder::orderBy('created_at','ASC');
            return datatables()->of($data)
            ->editColumn('client_order',function($data){
                return ucwords($data->client_order);
            })
            ->editColumn('total_price',function($data){
                return 'Rp.'.number_format($data->total_price);
            })
            ->editColumn('metode_bayar',function($data){
                $type = strtoupper(str_replace('_',' ',$data->metode_bayar));
                return $type;
            })
            ->editColumn('created_at',function($data){
                $date = Carbon::parse($data->created_at)->format('d M Y H:i:s');
                return $date;
            })
            ->editColumn('status',function($data){
                return strtoupper($data->status);
            })
            ->addColumn('action',function($data){
                if($data->status == 'cancel'){
                    
                }else{
                    if($data->status == 'finished'){
                        return '<div class="form-group text-center">
                        <a href="' . route('invoice.printInvoice', $data->invoice_number) . '" class="btn btn-success mr-1"><i class="fas fa-print"></i> Print</a>
                        <button type="button" data-invoice_no="'.$data->invoice_number.'" class="btn btn-warning deletes"><a><i class="fas fa-trash"></i> Cancel</a></button>
                        </div>';
                    }else{
                        return '<div class="form-group text-center">
                        <button type="button" data-invoice_no="'.$data->invoice_number.'" class="btn btn-danger deletes"><a><i class="fas fa-trash"></i> Delete</a></button>
                        </div>';
                    }
                }
            })
            ->rawColumns(['action'])
            ->make(true);
        }else{
            $data = [];
            return datatables()->of($data)
            ->make(true);
        }
    }

    public function printInvoice($id){
        $data = TransactionOrder::where('invoice_number',$id)->first();
        $pr_id = explode(',',$data->product_id);
        $detail = DB::table('master_product as mp')
        ->selectRaw('mb.description as product_name,mpr.description as process_name,mp.size,mp.total_price')
        ->join('master_barang as mb','mb.id','mp.barang_id')
        ->join('master_process as mpr','mpr.id','mp.process_id')
        ->whereIn('mp.id',$pr_id)
        ->get();
        // $pdf = PDF::loadview('transaction.invoice.print_invoice2', ['data'=> $data,'detail'=>$detail])->setPaper('legal','landscape');
        // return $pdf->stream();
        return view('transaction.invoice.print_invoice',compact('data','detail'));
    }
    public function deleteInvoice($id){
        $invoice = $id;
        try {
            DB::beginTransaction();
                $cek = TransactionOrder::where('invoice_number',$invoice)->first();
                if($cek != null){
                    return response()->json($invoice,331);
                }
                TransactionOrder::where('invoice_number',$invoice)->update([
                    'deleted_at'        => Carbon::now(),
                    'deleted_by'        => Auth::user()->id
                ]);
            DB::commit();
            return response()->json(200);
        }catch (\Exception $e){
            DB::rollback();
            $message = $e->getMessage();
            return response()->json($message, 500);
        }
    }

    public function cancelOrder(Request $request){
        $invoice = $request->invoice;
        try {
            DB::beginTransaction();
                TransactionOrder::where('invoice_number',$invoice)->update([
                    'status'            => 'cancel',
                    'deleted_at'        => Carbon::now(),
                    'deleted_by'        => Auth::user()->id
                ]);
                TransactionProduction::where('invoice_number',$invoice)->update([
                    'status'        => 'cancel',
                    'deleted_at'    => Carbon::now(),
                    'deleted_by'    => Auth::user()->id
                ]);
            DB::commit();
            return response()->json(200);
        }catch (\Exception $e){
            DB::rollback();
            $message = $e->getMessage();
            return response()->json($message, 500);
        }
    }
}
