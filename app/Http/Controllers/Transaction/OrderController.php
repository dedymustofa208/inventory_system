<?php

namespace App\Http\Controllers\Transaction;

use DB;
use Auth;
use Carbon;
use Config;
use DataTables;
use Illuminate\Support\Str;
use App\Models\MasterBarang;
use Illuminate\Http\Request;
use App\Models\MasterProduct;
use App\Models\TransactionOrder;
use App\Http\Controllers\Controller;
use App\Models\TransactionOrderDetail;

class OrderController extends Controller
{
    public function index(){
        $product = MasterProduct::whereNull('deleted_at')->orderBy('barang_id')->get();
        $type_bayar = Config::get('constants.type_bayar');
        return view('transaction.order.index',compact('product','type_bayar'));
    }

    public function addOrder(Request $request){
        $this->validate($request, [
            'client_name'   => 'required|min:3',
            'no_telp'       => 'nullable|string',
            'barang'        => 'required|array',
            'qty'           => 'required|array',
            'type_bayar'    => 'required'
        ]);
        $cn = Str::of($request->client_name)->lower()->trim();
        $tlp = Str::of($request->no_telp)->lower()->trim();
        try {
            DB::beginTransaction();
                // PAJAK 11% DASAR UU PPh No. 36 Tahun 2008.
                $order = TransactionOrder::create([
                    'client_order'      => $cn,
                    'no_telp'           => $tlp,
                    'pajak'             => 0,
                    'metode_bayar'      => $request->type_bayar,
                    'diskon'            => $request->diskon == null || $request->diskon == '0' ? '0' : $request->diskon,
                    'bonus'             => $request->bonus == null || $request->bonus == '0' ? '0' : $request->bonus,
                    'alamat'            => $request->alamat == null ? null : trim($request->alamat),
                    'catatan'           => $request->catatan == null ? null : trim($request->catatan),
                    'invoice_number'    => $request->invoice_number,
                    'created_by'        => Auth::user()->id,
                    'created_at'        => Carbon::now()
                ]);
                if($order)
                {
                    $qty = $request->qty;
                    $totalPrice =[];
                    $totalQty = [];
                    $detail = [];
                    foreach($request->barang as $index => $val) {
                        $price = MasterProduct::where('id', $val)->first()->total_price;
                        $dc = TransactionOrderDetail::create([
                            'order_id' => $order->id,
                            'qty' => $qty[$index],
                            'unit_price' => $price,
                            'created_by' => Auth::user()->id,
                            'created_at' => now(),
                            'product_id' => $val
                        ]);
                        $detail[] = $dc->id;
                        $totalPrice[] = (float)$price;
                        $totalQty[] = (int)$qty[$index];
                    }
                    if(array_sum($detail) > 0) {
                        TransactionOrder::where('id', $order->id)->update([
                            'qty' => array_sum($totalQty),
                            'total_price' => array_sum($totalPrice)
                        ]);
                    }
                }
            DB::commit();
            return response()->json(200);
        }catch (\Exception $e){
            DB::rollback();
            $message = $e->getMessage();
            return response()->json($message, 500);
        }
    }

    public function totalPrice(Request $request){
        $barang_id  = $request->barang;
        $qty        = $request->qty;
        $product_price = MasterProduct::where('id',$barang_id)->sum('total_price');
        $total_price = 0;
        if((int)$qty >= 10000){
            $total_price = ((0.11 * $product_price) + $product_price) * (int)$qty;
        }else{
            $total_price = $product_price * (int)$qty;
        }
        return response()->json($total_price,200);
    }

    public function totalBasedDisc(Request $request){
        $barang_id  = $request->barang;
        $qty        = $request->qty;
        $diskon     = $request->diskon;
        $product_price = MasterProduct::whereIn('id',$barang_id)->sum('total_price');
        if((int)$diskon > 0){
            $total_price = ($product_price - (((int)$diskon / 100) * $product_price)) * (int)$qty;
        }else{
            $total_price = $product_price * (int)$qty;
        }
        return response()->json($total_price,200);
    }

    public function genInvoice(Request $request){
        $type_bayar = $request->type_bayar;
        if($type_bayar == 'cash'){
            $code   = 'CSH';
        }elseif($type_bayar == 'debit'){
            $code   = 'DB';
        }else{
            $code   = 'CC';
        }
        if($type_bayar == '' || $type_bayar == null){
            $invoice_number = '';
        }else{
            $counter = 1;
            do {
                $invoice_number = 'INV'.Carbon::now()->format('u').$code.str_pad($counter, 2, '0', STR_PAD_LEFT);
                $invoice_number_exists = TransactionOrder::where('invoice_number',$invoice_number)->first();
                $counter++;
            } while ($invoice_number_exists != null);
        }
        return response()->json($invoice_number,200);
    }

    public function dataOrder(Request $request){
        if($request->ajax()){
            $data = TransactionOrder::whereNull('deleted_at')->orderBy('created_at','ASC');
            return datatables()->of($data)
            ->editColumn('client_order',function($data){
                return ucwords($data->client_order);
            })
            ->editColumn('product_id',function($data){
                $nm = DB::table('transaction_order_details as tod')
                        ->selectRaw("CONCAT(mb.description, '::', mpr.description, '::', mp.size) as products")
                        ->leftJoin('master_product as mp','tod.product_id','mp.id')
                        ->leftJoin('master_barang as mb', 'mb.id', 'mp.barang_id')
                        ->leftJoin('master_process as mpr', 'mpr.id','mp.process_id')
                        ->whereNull('mb.deleted_at')
                        ->whereNull('mp.deleted_at')
                        ->where('tod.order_id', $data->id)->pluck('products')->toArray();
                $productName = Str::of(implode(' | ', $nm))->upper();
                return $productName;
            })
            ->editColumn('qty',function($data){
                return $data->qty.' Pcs';
            })
            ->editColumn('total_price',function($data){
                return 'Rp. '.number_format($data->total_price);
            })
            ->editColumn('metode_bayar',function($data){
                $type = strtoupper(str_replace('_',' ',$data->metode_bayar));
                return $type;
            })
            ->editColumn('created_at',function($data){
                $date = Carbon::parse($data->created_at)->format('d M Y H:i:s');
                return $date;
            })
            ->editColumn('is_completed',function($data){
                $cond = $data->is_completed == false ? 'Belum Lunas' : 'Lunas';
                return $cond;
            })
            ->rawColumns([])
            ->make(true);
        }else{
            $data = [];
            return datatables()->of($data)
            ->make(true);
        }
    }
}
