<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\BahanBaku;
use App\Models\StockBahan;
use App\Models\MasterSupplier;
use DataTables;
use Auth;
use Config;
use Carbon;

class StockController extends Controller
{
    public function index(){
        $supplier = MasterSupplier::whereNull('deleted_at')->orderBy('description')->get();
        $bahan_baku = BahanBaku::whereNull('deleted_at')->orderBy('bahan_type')->orderBy('description')->get();
        return view('transaction.stock.index',compact('supplier','bahan_baku'));
    }

    public function dataStock(Request $request){
        if($request->ajax()){
            $data = StockBahan::orderByRaw("updated_at IS NULL, updated_at ASC")->whereNull('deleted_at');
            return DataTables::of($data)
            ->addColumn('action',function($data){
                return '<div class="btn-group">
                    <button type="button" data-id="'.$data->id.'" class="btn btn-info edit mx-sm-1"><a><i class="fas fa-edit"></i> Edit</a></button>
                </div>';
            })
            ->editColumn('bahan_id',function($data){
                return strtoupper($data->namaBahan->description.' - '.$data->namaBahan->code_bahan);
            })
            ->editColumn('qty',function($data){
                return number_format($data->qty);
            })
            ->editColumn('qty_used',function($data){
                return number_format($data->qty_used);
            })
            ->editColumn('qty_available',function($data){
                return number_format($data->qty_available);
            })
            ->editColumn('pemasok',function($data){
                $supplier_name = MasterSupplier::where('id',$data->pemasok)->first();
                return strtoupper($supplier_name->description);
            })
            ->editColumn('created_at',function($data){
                return Carbon::parse($data->created_at)->format('d M Y H:i:s');
            })
            ->rawColumns(['action'])
            ->make(true);
        }else{
            $data = [];
            return datatables()->of($data)
            ->make(true);
        }
    }

    public function addStock(Request $request){
        $this->validate($request, [
            'qty_stock' => 'required',
        ]);
        try{
            DB::begintransaction();
                StockBahan::insertGetId([
                    'bahan_id' => $request->bahan_baku,
                    'qty' => (int)$request->qty_stock,
                    'qty_used' => 0,
                    'qty_available' => (int)$request->qty_stock,
                    'pemasok' => $request->supplier,
                    'created_by' => Auth::user()->id,
                    'created_at' => Carbon::now()
                ]);
            DB::commit();
            return response()->json('Stock Saved',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }

    public function editStock($id){
        $data = StockBahan::where('id',$id)->first();
        if($data->qty_used > 0){
            return response()->json('Stock Sudah Digunakan Anda Tidak Boleh merubahnya!',422);
        }
        return response()->json($data,200);
    }

    public function updateStock(Request $request){
        $this->validate($request, [
            'qty_stock' => 'required'
        ]);
        $id = $request->id_update;

        try{
            DB::begintransaction();
                StockBahan::where('id',$id)->update([
                    'bahan_id' => $request->bahan_baku,
                    'qty' => (int)$request->qty_stock,
                    'qty_used' => 0,
                    'qty_available' => (int)$request->qty_stock,
                    'pemasok' => $request->supplier,
                    'uom' => $request->uom,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => Carbon::now()
                ]);
            DB::commit();
            return response()->json('Barang Updated',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }
}
