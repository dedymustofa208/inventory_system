<?php

namespace App\Http\Controllers\Master;

use DB;
use Auth;
use Carbon;
use Config;
use DataTable;
use App\Models\BahanBaku;
use App\Models\StockBahan;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BahanBakuController extends Controller
{
    public function index(){
        $type = Config::get('constants.bahan_type');
        $warna = Config::get('constants.warna');
        $uom = Config::get('constants.uom');
        return view('master.bahan_baku.index',compact('type','warna','uom'));
    }

    public function dataBahanBaku(Request $request){
        if($request->ajax()){
            $data = BahanBaku::orderbyRaw('bahan_type,description');
            return datatables()->of($data)
            ->addColumn('action',function($data){
                return '<div class="btn-group">
                    <button type="button" data-id="'.$data->id.'" class="btn btn-info edit mx-sm-1"><a><i class="fas fa-edit"></i> Edit</a></button>
                
                    <button type="button" data-id="'.$data->id.'" class="btn btn-danger deletes"><a><i class="fas fa-trash"></i> Delete</a></button>
                </div>';
            })
            ->editColumn('description',function($data){
                return strtoupper($data->description);
            })
            ->editColumn('bahan_type',function($data){
                return strtoupper($data->bahan_type);
            })
            ->editColumn('code_bahan',function($data){
                return strtoupper($data->code_bahan);
            })
            ->editColumn('color_name',function($data){
                return Str::of($data->color_name)->replace('_',' ')->headline();
            })
            ->editColumn('unit_prices', function ($data) {
                return 'Rp. '.number_format($data->unit_prices);
            })
            ->rawColumns(['action'])
            ->make(true);
        }else{
            $data = [];
            return datatables()->of($data)
            ->make(true);
        }
    }

    public function addBahanBaku(Request $request){
        $this->validate($request, [
            'nama_bahan' => 'required|string',
            'bahan_type' => 'required|string',
            'type_warna' => 'required|string',
            'uoms' => 'required|string'
        ]);
        $nama_bahan = Str::of($request->nama_bahan)->lower()->trim();
        $warna = Str::of($request->type_warna)->lower()->trim();
        $bahan_type = Str::of($request->bahan_type)->lower()->trim();
        $uom = Str::of($request->uoms)->lower()->trim();
        $harga = (float)$request->harga_barang;
        $code = $this->genCode();
        $basedUom = Config::get('constants.uom_based');
        foreach($basedUom as $key => $val) {
            if($bahan_type == $key && !in_array($uom, $val)) {
                return response()->json('Satuan Tidak Sesuai Dengan Bahan!', 422);
            }
        }

        try{
            DB::begintransaction();
                BahanBaku::insertGetId([
                    'description' => $nama_bahan,
                    'code_bahan' => $code,
                    'color_name' => $warna,
                    'bahan_type' => $request->bahan_type,
                    'unit_prices' => $harga,
                    'uom' => $uom,
                    'created_by' => Auth::user()->id,
                    'created_at' => Carbon::now()
                ]);
            DB::commit();
            return response()->json('Data Berhasil Disimpan!',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,500);
        }

    }

    public function editBahanBaku($id){
        $data = BahanBaku::where('id',$id)->first();
        return response()->json($data,200);
    }

    public function deleteBahanBaku($id){
        try{
            DB::begintransaction();
                $cek = StockBahan::where('bahan_id',$id)->first();
                if($cek != null){
                    return response()->json('Bahan Baku Tidak Boleh Dihapus!',422);
                }
                BahanBaku::where('id',$id)->update([
                    'deleted_at'    => Carbon::now(),
                    'deleted_by'    => Auth::user()->id
                ]);
            DB::commit();
            return response()->json('Bahan Baku Deleted',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }

    public function updateBahanBaku(Request $request){
        $this->validate($request, [
            'nama_bahan' => 'required',
        ]);
        $id = $request->id_update;
        $nama_bahan = trim(strtolower($request->nama_bahan));

        try{
            DB::begintransaction();
                BahanBaku::where('id',$id)->update([
                    'description' => $nama_bahan,
                    'color_name' => $request->type_warna,
                    'bahan_type' => $request->bahan_type,
                    'uom' => $request->uoms,
                    'unit_prices' => (int)$request->harga_barang,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => Carbon::now()
                ]);
            DB::commit();
            return response()->json('Bahan Baku Updated',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }

    private function genCode()
    {
        do {
            $kode = Str::upper(Str::random(6));
        } while (BahanBaku::where('code_bahan', $kode)->exists());

        return $kode;
    }
}
