<?php

namespace App\Http\Controllers\Master;

use DB;
use Auth;
use File;
use Carbon;
use Config;
use DataTables;
use App\Models\MasterSize;
use App\Models\MasterBarang;
use Illuminate\Http\Request;
use App\Models\MasterProcess;
use App\Models\MasterProduct;
use App\Models\ProdukBomDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    public function index(){
        $barang = MasterBarang::whereNull('deleted_at')->orderBy('description')->get();
        $process = MasterProcess::whereNull('deleted_at')->get();
        $size = MasterSize::whereNull('deleted_at')->get();
        $bahan_type = Config::get('constants.bahan_type');
        $uom = Config::get('constants.uom_bom');
        return view('master.product.index',compact('barang','process','size','bahan_type','uom'));
    }

    public function dataProduct(Request $request){
        if($request->ajax()){
            $data = MasterProduct::whereNull('deleted_at')->orderBy('created_at','DESC');
            return datatables()->of($data)
            ->addColumn('action',function($data){
                return '<div class="text-center">                
                            <button type="button" data-id="'.$data->id.'" class="btn btn-danger deletes"><a><i class="fas fa-trash"></i> Delete</a></button>
                        </div>';
            })
            ->editColumn('barang_id',function($data){
                return strtoupper($data->barang->description);
            })
            ->editColumn('process_id',function($data){
                return strtoupper($data->process->description);
            })
            ->editColumn('photo_product', function ($data) {
                if($data->photo_product == null){
                    return "<div class='text-center'>
                                <img alt='product' src=".asset('admin_asset/img/boxed-bg.jpg')." class='table-avatar' style='width: 50%; height: 50%;' src='%s'>
                            </div>";
                }else{
                    $imagePath = url($data->photo_product);
                    return "<div class='text-center'>
                    <img alt='product' class='table-avatar' style='width: 50%; height: 50%;' src='$imagePath'>
                    </div>";
                }
            })
            ->editColumn('size',function($data){
                return strtoupper($data->size);
            })
            ->editColumn('total_price',function($data){
                return 'Rp.'.number_format($data->total_price);
            })
            ->rawColumns(['action','photo_product'])
            ->make(true);
        }else{
            $data = [];
            return datatables()->of($data)
            ->make(true);
        }
    }

    public function addProduct(Request $request)
    {
        $this->validate($request, [
            'barang' => 'required',
            'process' => 'required',
            'size' => 'required',
            'photo_product' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'tipeBahan' => 'required|array',
            'uom' => 'required|array',
            'qty' => 'required|array',
            'tipeBahan.*' => 'required|string',
            'uom.*' => 'required|string',
            'qty.*' => 'required|numeric|min:0'
        ]);

        $barang = $request->barang;
        $process = explode(',',$request->process);
        $size = explode(',', $request->size);
        $tipeBahan = $request->tipeBahan;
        $uom = $request->uom;
        $qty = $request->qty;

        try {
            DB::beginTransaction();

            if ($request->hasFile('photo_product')) {
                $photo = $request->file('photo_product');
                $originalName = $photo->getClientOriginalName();
                $filename = pathinfo($originalName, PATHINFO_FILENAME);
                $extension = $photo->getClientOriginalExtension();
                $imagePathArray = [];

                foreach ($size as $index => $s) {
                    $increment = $index + 1;
                    $filenameWithIncrement = $filename . '_' . $increment . '.' . $extension;
                    $path = $photo->storeAs('public/img/photo_product', $filenameWithIncrement);
                    $imagePath = str_replace('public/', 'storage/', $path);
                    $imagePathArray[] = $imagePath;
                    $image = Image::make(storage_path('app/' . $path));
                    $image->save();
                }
            } else {
                $imagePathArray = array_fill(0,count($size), null);
            }
            $pb = MasterBarang::where('id', $barang)->pluck('price')->first();
            foreach ($size as $index => $s) {
                $ps = MasterSize::where('description', $s)->value('additional_price');
                foreach ($process as $p) {
                    $pp = MasterProcess::where('id', $p)->value('price');
                    // Pastikan $barang adalah objek, bukan hanya ID
                    if (is_numeric($barang)) {
                        $barang = MasterBarang::findOrFail($barang); // Ambil objek barang jika hanya ID
                    }
                    $exists = MasterProduct::where([
                        ['barang_id', $barang->id],
                        ['process_id', $p],
                        ['size', $s]
                    ])->exists();
                    if ($exists) {
                        return response()->json('Barang ' . strtoupper($barang->description) . 
                            ' Proses ' . strtoupper($p->description) . 
                            ' Size: <b>' . strtoupper($s) . '</b> Sudah Pernah Dibuat!', 422);
                    }
                    $total_price = (float)$pb + (float)$pp + (float)$ps;
                    $product_id = MasterProduct::insertGetId([
                        'barang_id' => (int)$barang->id,
                        'process_id' => $p,
                        'size' => $s,
                        'total_price' => $total_price,
                        'photo_product' => $imagePathArray[$index] ?? null, // Hindari error jika index tidak ada
                        'created_by' => Auth::id(),
                        'created_at' => now()
                    ]);
            
                    // Inisialisasi BOM
                    $bomData = [];
                    $cekProcess = MasterProcess::findOrFail($p);
                    $createdBy = Auth::id();
                    $createdAt = now();
            
                    foreach ($tipeBahan as $idx => $type) {
                        if ($type === 'benang' && $cekProcess->description === 'embro') {
                            ProdukBomDetail::create([
                                'product_id' => $product_id,
                                'material' => $type,
                                'uom' => $uom[$idx],
                                'cons' => $qty[$idx],
                                'created_by' => $createdBy,
                                'created_at' => $createdAt
                            ]);
                            continue;
                        }
            
                        if ($cekProcess->description !== 'embro' && $type !== 'benang') {
                            $bomData[] = [
                                'product_id' => $product_id,
                                'material' => $type,
                                'uom' => $uom[$idx],
                                'cons' => $qty[$idx],
                                'created_by' => $createdBy,
                                'created_at' => $createdAt
                            ];
                        }
                    }
                    if (!empty($bomData)) {
                        collect($bomData)->chunk(100)->each(function ($chunk) {
                            ProdukBomDetail::insert($chunk->toArray());
                        });
                    }
                }
            }            

            DB::commit();
            return response()->json('Product Saved!', 200);
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            return response()->json($message, 500);
        }
    }

    public function deleteProduct($id){
        try{
            DB::begintransaction();
            $product = MasterProduct::where('id',$id)->update([
                'deleted_at'    => Carbon::now(),
                'deleted_by'    => Auth::user()->id
            ]);
            // $photoProduct = $product->photo_product;
            //     if ($photoProduct) {
            //         Storage::delete($photoProduct);
            //         $publicPath = public_path($photoProduct);
            //         if (File::exists($publicPath)) {
            //             File::delete($publicPath);
            //         }
            //     }
            DB::commit();
            return response()->json('Product Deleted',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }
}
