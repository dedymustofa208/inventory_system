<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterProcess;
use Auth;
use Carbon\Carbon;
use DB;
use Datatables;

class ProcessController extends Controller
{
    public function index(){
        return view('master.process.index');
    }

    public function store(Request $request){          
        $this->validate($request, [
            'nama_process' => 'required'
        ]);

        $nama_proses = trim(strtolower($request->nama_process));
        $price = $request->price;

        try {
            DB::beginTransaction();

            $cek = MasterProcess::where('description', $nama_proses)->first();            
            if($cek == null){
                $new = [                    
                    'description' => $nama_proses,
                    'price'       => (float)$price,
                    'created_at'  => now(),
                    'created_by'  => Auth::user()->id
                ];                

                MasterProcess::firstOrcreate($new);
                DB::commit();
                return response()->json('Add Process Success!', 200);
            }
        } catch(\Exception $e){
            DB::rollback();
            $message = $e->getMessage();
            return response()->json($message,500);
        }
    }

    public function data_process(){
        $data = MasterProcess::whereNull('deleted_at')->orderBy('created_at', 'desc');
        
        return datatables()->of($data)
        ->addColumn('action',function($data){
            return '<div class="btn-group">
            
                <button type="button" data-id="'.$data->id.'" data-name="'.$data->description.'" data-price="'.$data->price.'" class="btn btn-info edit mx-sm-1"><a><i class="fas fa-edit"></i> Edit</a></button>
            
                <button type="button" data-id="'.$data->id.'" data-name="'.$data->description.'" class="btn btn-danger delete"><a><i class="fas fa-trash"></i> Delete</a></button>
            </div>';
        })
        ->editColumn('description',function($data){
            return strtoupper($data->description);
        })
        ->editColumn('price',function($data){
            return 'Rp.'.number_format($data->price);
        })        
        ->make(true);
    }

    public function update_data(Request $request){        
        $this->validate($request, [
            'nama_process' => 'required',
            'id' => 'required'
        ]);

        $id          = $request->id;
        $nama_proses = $request->nama_process;
        $price       = $request->price;
        
        try {
            DB::beginTransaction();
            
            $update = MasterProcess::where('id', $id)->update([
                'description'   => $nama_proses,
                'price'         => $price,
                'updated_at'    => now(),
                'updated_by'    => Auth::user()->id
            ]);

            DB::commit();
            return response()->json('Update Process Success!', 200);
        
        } catch(\Exception $e){
            DB::rollback();
            $message = $e->getMessage();
            return response()->json($message,500);
        }
    }

    public function delete_data(Request $request){
        $id = $request->id;
        // dd($id);
        try {
            DB::beginTransaction();

            MasterProcess::where('id', $id)->update([
                'deleted_by' => Auth::user()->id,
                'updated_at' => now()
            ]);
            MasterProcess::where('id', $id)->delete();

            DB::commit();
            return response()->json('Delete Process Success!', 200);
        
        } catch(\Exception $e){
            DB::rollback();
            $message = $e->getMessage();
            return response()->json($message,500);
        }

    }
}
