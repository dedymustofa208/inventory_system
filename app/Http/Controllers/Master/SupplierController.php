<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterSupplier;
use Auth;
use Carbon\Carbon;
use DB;
use DataTables;

class SupplierController extends Controller
{
    public function index(){
        return view('master.supplier.index');
    }

    public function dataSupplier(Request $request){
        if($request->ajax()){
            $data = MasterSupplier::orderby('created_at','desc')->whereNull('deleted_at');
            return datatables()->of($data)
            ->addColumn('action',function($data){
                return '<div class="btn-group">
                    <button type="button" data-id="'.$data->id.'" class="btn btn-info edit mx-sm-1"><a><i class="fas fa-edit"></i> Edit</a></button>
                
                    <button type="button" data-id="'.$data->id.'" class="btn btn-danger deletes"><a><i class="fas fa-trash"></i> Delete</a></button>
                </div>';
            })
            ->editColumn('description',function($data){
                return strtoupper($data->description);
            })
            ->rawColumns(['action'])
            ->make(true);
        }else{
            $data = [];
            return datatables()->of($data)
            ->make(true);
        }
    }

    public function addSupplier(Request $request){
        $this->validate($request, [
            'nama_supplier' => 'required',
        ]);
        $barang = trim(strtolower($request->nama_supplier));
        $validation  = MasterSupplier::where('description',$barang)->whereNull('deleted_at')->first();
        if($validation != null){
            return response()->json('Barang Sudah Ada',422);
        }
        try{
            DB::begintransaction();
                MasterSupplier::insertGetId([
                    'description'   => $barang,
                    'created_by'    => Auth::user()->id,
                    'created_at'    => Carbon::now()
                ]);
            DB::commit();
            return response()->json('Supplier Saved',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }

    }

    public function editSupplier($id){
        $data = MasterSupplier::where('id',$id)->first();
        return response()->json($data,200);
    }

    public function deleteSupplier($id){
        try{
            DB::begintransaction();
                MasterSupplier::where('id',$id)->update([
                    'deleted_at'    => Carbon::now(),
                    'deleted_by'    => Auth::user()->id
                ]);
            DB::commit();
            return response()->json('Barang Deleted',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }

    public function updateSupplier(Request $request){
        $this->validate($request, [
            'nama_supplier_update'   => 'required',
            'id_update'     => 'required'
        ]);
        $id = $request->id_update;
        $nama_supplier = $request->nama_supplier_update;

        $validation = MasterSupplier::where('description',$nama_supplier)->whereNull('deleted_at')->first();
        if($validation != null){
            return response()->json('Nama Supplier Sudah Ada Gunakan Nama Yang lain!',422);
        }
        try{
            DB::begintransaction();
                MasterSupplier::where('id',$id)->update([
                    'description'   => trim(strtolower($nama_supplier)),
                    'updated_by'    => Auth::user()->id,
                    'updated_at'    => Carbon::now()
                ]);
            DB::commit();
            return response()->json('Supplier Updated',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }
}
