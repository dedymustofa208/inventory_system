<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterBarang;
use Auth;
use Carbon\Carbon;
use DB;
use DataTables;

class BarangController extends Controller
{
    public function index(){
        return view('master.barang.index');
    }

    public function dataBarang(Request $request){
        if($request->ajax()){
            $data = MasterBarang::orderby('created_at','desc')->whereNull('deleted_at');
            return datatables()->of($data)
            ->addColumn('action',function($data){
                return '<div class="btn-group">
                    <button type="button" data-id="'.$data->id.'" class="btn btn-info edit mx-sm-1"><a><i class="fas fa-edit"></i> Edit</a></button>
                
                    <button type="button" data-id="'.$data->id.'" class="btn btn-danger deletes"><a><i class="fas fa-trash"></i> Delete</a></button>
                </div>';
            })
            ->editColumn('description',function($data){
                return strtoupper($data->description);
            })
            ->editColumn('price',function($data){
                return 'Rp.'.number_format($data->price);
            })
            ->rawColumns(['action'])
            ->make(true);
        }else{
            $data = [];
            return datatables()->of($data)
            ->make(true);
        }
    }

    public function addBarang(Request $request){
        $this->validate($request, [
            'nama_barang' => 'required|min:3',
            'price' => 'required'
        ]);
        $barang = trim(strtolower($request->nama_barang));
        $price = $request->price;
        $validation  = MasterBarang::where('description',$barang)->whereNull('deleted_at')->first();
        if($validation != null){
            return response()->json('Barang Sudah Ada',422);
        }
        try{
            DB::begintransaction();
                MasterBarang::insertGetId([
                    'description'   => $barang,
                    'price'         => (float)$price,
                    'created_by'    => Auth::user()->id,
                    'created_at'    => Carbon::now()
                ]);
            DB::commit();
            return response()->json('Role Saved',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }

    }

    public function editBarang($id){
        $data = MasterBarang::where('id',$id)->first();
        return response()->json($data,200);
    }

    public function deleteBarang($id){
        try{
            DB::begintransaction();
                MasterBarang::where('id',$id)->update([
                    'deleted_at'    => Carbon::now(),
                    'deleted_by'    => Auth::user()->id
                ]);
            DB::commit();
            return response()->json('Barang Deleted',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }

    public function updateBarang(Request $request){
        $this->validate($request, [
            'nama_barang'   => 'required|min:3',
            'harga_barang'  => 'required',
            'id_update'     => 'required'
        ]);
        $id = $request->id_update;
        $nama_barang = $request->nama_barang;
        $harga_barang = $request->harga_barang;

        // $validation = MasterBarang::where('description',$nama_barang)->whereNull('deleted_at')->first();
        // if($validation != null){
        //     return response()->json('Nama Barang Sudah Ada Gunakan Nama Yang lain!',422);
        // }
        try{
            DB::begintransaction();
                MasterBarang::where('id',$id)->update([
                    'description'   => trim(strtolower($nama_barang)),
                    'price'         => (float)$harga_barang,
                    'updated_by'    => Auth::user()->id,
                    'updated_at'    => Carbon::now()
                ]);
            DB::commit();
            return response()->json('Barang Updated',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }
}
