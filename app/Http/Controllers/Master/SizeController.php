<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterSize;
use DB;
use DataTables;
use Auth;
use Carbon;

class SizeController extends Controller
{
    public function index(){
        return view('master.size.index');
    }

    public function dataSize(Request $request){
        if($request->ajax()){
            $data = MasterSize::orderby('created_at','desc');
            return datatables()->of($data)
            ->addColumn('action',function($data){
                return '<div class="btn-group">
                    <button type="button" data-id="'.$data->id.'" class="btn btn-info edit mx-sm-1"><a><i class="fas fa-edit"></i> Edit</a></button>
                
                    <button type="button" data-id="'.$data->id.'" class="btn btn-danger deletes"><a><i class="fas fa-trash"></i> Delete</a></button>
                </div>';
            })
            ->editColumn('additional_price',function($data){
                return 'Rp.'.number_format($data->additional_price);
            })
            ->editColumn('description',function($data){
                return strtoupper($data->description);
            })
            ->rawColumns(['action'])
            ->make(true);
        }else{
            $data = [];
            return datatables()->of($data)
            ->make(true);
        }
    }

    public function addSize(Request $request){
        $this->validate($request, [
            'size' => 'required'
        ]);
        $size = trim(strtolower($request->size));
        $additional_price = $request->additional_price == null ? '0' : $request->additional_price;
        $validation  = MasterSize::where('description',$size)->whereNull('deleted_at')->first();
        if($validation != null){
            return response()->json('Size Sudah Ada',422);
        }
        try{
            DB::begintransaction();
                MasterSize::insertGetId([
                    'description'       => $size,
                    'additional_price'  => $additional_price,
                    'created_by'        => Auth::user()->id,
                    'created_at'    => Carbon::now()
                ]);
            DB::commit();
            return response()->json('Size Saved',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }

    }

    public function editSize($id){
        $data = MasterSize::where('id',$id)->first();
        return response()->json($data,200);
    }

    public function deleteSize($id){
        try{
            DB::begintransaction();
                MasterSize::where('id',$id)->update([
                    'deleted_at'        => Carbon::now(),
                    'deleted_by'        => Auth::user()->id
                ]);
            DB::commit();
            return response()->json('Size Deleted',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }

    public function updateSize(Request $request){
        $this->validate($request, [
            'size_update'   => 'required',
            'id_update'     => 'required'
        ]);
        $id = $request->id_update;
        $size = $request->size_update;
        $additional_price = $request->additional_price_update == null ? '0' : $request->additional_price_update;

        $validation = MasterSize::where('description',$size)->where('additional_price',$additional_price)->whereNull('deleted_at')->first();
        if($validation != null){
            return response()->json('Size Sudah Ada Gunakan Nama Yang lain!',422);
        }
        try{
            DB::begintransaction();
                MasterSize::where('id',$id)->update([
                    'description'   => trim(strtolower($size)),
                    'additional_price'  => $additional_price,
                    'updated_by'    => Auth::user()->id,
                    'updated_at'    => Carbon::now()
                ]);
            DB::commit();
            return response()->json('Size Updated',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }
}
