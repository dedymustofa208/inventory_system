<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission; 
use Spatie\Permission\Traits\HasRoles;
use App\Models\User;
use DB;
use stdClass;
use Str;
use Carbon\Carbon;
use DataTables;

class RoleController extends Controller
{
    use HasRoles;

    public function index(){
        $permission = Permission::orderBy('name')->get();
        return view('admin.roles.index',compact('permission'));
    }

    public function getData()
    {
        if(request()->ajax()) 
        {
            $data = Role::orderby('created_at','desc')->get();
            return datatables()->of($data)
            ->addColumn('action',function($data){
                return '<div class="row">
                <button type="button" data-id="'.$data->id.'" class="btn btn-info edit mx-sm-1"><a><i class="fas fa-edit"></i> Edit</a></button>
                <button type="button" data-id="'.$data->id.'" class="btn btn-danger deletes"><a><i class="fas fa-trash"></i> Delete</a></button>
                </div>';
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function saveData(Request $request)
    {
        if(Role::where('name',Str::slug(trim(strtolower($request->role_name))))->exists()){
            return response()->json('Role sudah ada', 422);
        }
        try{
            DB::begintransaction();
            $role = Role::firstorCreate([
                'name'          => Str::slug(strtolower(trim($request->role_name))),
                'created_at'    => Carbon::now()
            ]);
            

            $perm_id   =  explode(',',$request->permission_name);
            $array      = array();

            foreach ($perm_id as $key => $p){
                $array [] = [ 'id' => $p ];
            }
            
            if($role->save()){
                $role->syncPermissions($array);
            } 
            DB::commit();
            return response()->json('Role Saved',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }

    public function editData($id)
    {
        $role               = Role::find($id);
        $name               = str_replace('-',' ',$role->name);
        $obj                = new StdClass();
        $obj->id            = $id;
        $obj->name          = $name;
        $obj->permission_id = implode(',',$role->permissions->pluck('id')->toArray());

		return response()->json($obj,200);
    }

    public function updateData(Request $request){
        try{
            DB::begintransaction();
            $role = Role::findOrFail($request->id_update);
            $role->where('id',$request->id_update)->update([
                'name'      => Str::slug(trim(strtolower($request->role_name_update)))
            ]);
            $perm_id = explode(',',$request->permission_name_update);
            
            $role->syncPermissions((int)$perm_id);
            DB::commit();
            return response()->json('Role Saved',200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message,422);
        }
    }

    public function destroy($id){

        try {
            DB::beginTransaction();
                
                $role = Role::findOrFail($id);
                if($role->name == 'superuser'){
                    return response()->json('This Role Cant be Deleted!',422);
                }
                $role->permissions()->detach();
                $role->delete();
            DB::commit();
            return response()->json('Role Deleted', 200);
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json($message, 422);
        }
    }
}
