<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MasterBarang;
use App\Models\MasterProduct;
use App\Models\TransactionOrder;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $barang         = MasterBarang::whereNull('deleted_at')->count('id');
        $prod_embro     = MasterProduct::whereNull('deleted_at')->where('process_id','1')->count('id');
        $prod_print     = MasterProduct::whereNull('deleted_at')->where('process_id','2')->count('id');
        $total_order    = TransactionOrder::whereNull('deleted_at')->sum('qty');
        $total_invoice    = TransactionOrder::count('invoice_number');
        $paid_invoice    = TransactionOrder::whereNull('deleted_at')->where('is_completed',TRUE)->count('invoice_number');
        $outstanding_invoice    = TransactionOrder::whereNull('deleted_at')->whereIn('status',['onprocess','draft'])->count('invoice_number');
        $cancel_order    = TransactionOrder::wherenotNull('deleted_at')->where('status','cancel')->count('invoice_number');
        $total_jual    = TransactionOrder::whereNull('deleted_at')->whereIn('status',['finish','onprocess'])->sum('total_price');
        $total_rugi    = TransactionOrder::whereNotNull('deleted_at')->sum('total_price');
        return view('home',compact('barang','prod_embro','prod_print','total_order','total_invoice','cancel_order','total_jual','total_rugi','paid_invoice','outstanding_invoice'));
    }

    public function urutanOrder(Request $request){
        if($request->ajax()){
            $data = TransactionOrder::whereNull('deleted_at')->orderBy('created_at');
            return datatables()->of($data)
            ->editColumn('client_order',function($data){
                return strtoupper($data->client_order);
            })
            ->editColumn('status',function($data){
                if($data->status == 'onprocess'){
                    return 'Dalam Proses Produksi';
                }elseif($data->status == 'finished'){
                    return 'Selesai';
                }else{
                    return 'Menunggu Proses Produksi';
                }
            })
            ->editColumn('created_at',function($data){
                return Carbon::parse($data->created_at)->format('D, d F Y | H:i:s');
            })
            ->make(true);
        }else{
            $data = [];
            return datatables()->of($data)
            ->make(true);
        }
    }
}
