<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use File;
use Config;
use StdClass;
use Validator;
use Carbon;
use Datatables;
use Auth;
use DB;
use App\Models\User;
use Ramsey\Uuid\Uuid;

class RegisterController extends Controller
{
    public function index(){
        $role = Role::orderBy('name')->get();
        $permission = Permission::get();
        return view('admin.register.index',compact('role','permission'));
    }

    public function data(Request $request){
        if(request()->ajax()) 
        {
            $data = User::orderby('created_at','desc')->get();
            return datatables()->of($data)
            ->addColumn('action',function($data){
                return '<div class="btn-group">                
                    <button type="button" data-id="'.$data->id.'" class="btn btn-danger deletes"><a><i class="fas fa-trash"></i> Delete</a></button>
                </div>';
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function store(Request $request){
        $email = $request->email_user;
        $gender = $request->gender;
        $username = $request->username;

        if(User::where('email',$email)->exists()){
            return response()->json('Email Sudah Terdaftar!',422);
        }

        if(User::where('name',$username)->exists()){
            return response()->json('Username Sudah Terdaftar!',422);
        }

        if($request->photo_user != null){
            $photo = $request->file('photo_user');
            $path = $photo->store('public/img/avatar');
            $image = Image::make(storage_path('app/' . $path))->fit(160, 160);
            $image->save();
        }else{
            $path = null;
        }

        $user = User::firstorCreate([
            'name'              => $username,
            'gender'            => $gender,
            'photo'             => $path,
            'email'             => $email,
            'email_verified_at' => Carbon::now(),
            'password'          => bcrypt($request->password),
            'created_at'        => Carbon::now()
        ]);
        
        if($user->save()){
            $user->assignRole($request->role_user);
        }
        return response()->json('Register Successfully!',200);
    }

    public function edit($id){
        $data               = User::find($id);
		return response()->json($data,200);
    }

    public function update(Request $request){
        dd($request->all());
    }
    public function delete($id){
        dd($request->all());
    }
}
