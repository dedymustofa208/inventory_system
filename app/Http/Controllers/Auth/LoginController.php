<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Session;
use Carbon\Carbon;

use App\Models\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/home';

    protected $email;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->email = $this->findEmail();
    }

    public function findEmail()
    {
        $email = request()->input('email');
        request()->merge(['email' => $email]);
        return 'email';
    }

    public function username()
    {
        return $this->email;
    }

    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);

        $credentials = $this->credentials($request);

        if(isset($credentials['email'])){
            $not_found  = User::where('email',$credentials['email'])->count();
            $info  = User::where('email',$credentials['email'])->first();
        }

        if($info != null && in_array('superuser',$info->getRoleNames()->toArray()) == true){
            if (Auth::validate($credentials)) {
                $user = Auth::getLastAttempted();
                Auth::login($user, $request->has('remember'));
                User::where('id',Auth::user()->id)->update([
                    'updated_at'    => Carbon::now()
                ]);
                return redirect()->intended('/home');

            }

            if(isset($credentials['user_email'])){
                return redirect('/login')
                ->withInput($request->only($credentials['email'], 'remember'))
                ->withErrors([
                    'email' => 'Email atau Password anda salah',
                ]);
            }
        }

        if (Auth::validate($credentials)) {
            $user = Auth::getLastAttempted();
            Auth::login($user, $request->has('remember'));
            User::where('id',Auth::user()->id)->update([
                'updated_at'    => Carbon::now()
            ]);
            return redirect()->intended('/home');

        }elseif($not_found == 0 && $info == null){
            Session::flash("flash_notification", ["message"=> "Email Tidak Terdaftar!"]);
            return  redirect()
            ->back()
            ->withInput($request->only($this->username(), 'remember'));
        }else{
            Session::flash("flash_notification", ["message"=> "Email TIdak Terdaftar"]);
            return  redirect()
            ->back()
            ->withInput($request->only($this->username(), 'remember'));
        }
    }
}
