<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel;
use App\Exports\Report\LaporanTransaksi;
use App\Exports\Report\LaporanTagihan;
use App\Exports\Report\LaporanStock;
use Carbon\Carbon;
use DB;

class ReportsController extends Controller
{
    public function transaksi(){
        return view('report.transaksi.index');
    }

    public function downloadTransaksi(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $data = DB::table('v_report_transaksi')->whereBetween('order_date',[$start_date,$end_date])->get();
        $filename = 'Laporan_Transaksi_from_'.$start_date.'_to_'.$end_date.'.xlsx';
        return Excel::download(new LaporanTransaksi($data), $filename);
    }

    public function tagihan(){
        return view('report.tagihan.index');
    }

    public function downloadTagihan(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $data = DB::table('v_report_transaksi')
        ->select(DB::raw("client_order,invoice_number,qty,GROUP_CONCAT(nama_barang SEPARATOR ',') as nama_barang,size,process_name,total_price,status,order_date,created_by,finish_date"))
        ->whereBetween('finish_date',[$start_date,$end_date])
        ->where('status','finished')
        ->groupByRaw('client_order,invoice_number,qty,size,process_name,total_price,status,order_date,created_by,finish_date')
        ->get();
        $filename = 'Laporan_Tagihan_from_'.$start_date.'_to_'.$end_date.'_.xlsx';
        return Excel::download(new LaporanTagihan($data), $filename);
    }

    public function stock(){
        return view('report.stock.index');
    }

    public function downloadStockBahan(Request $request){
        $data = DB::table('v_stock_bahan')->orderBy('bahan_type')->orderBy('tanggal_beli')->get();
        $filename = 'Laporan_Stock.xlsx';
        return Excel::download(new LaporanStock($data), $filename);
    }
}
