<?php

namespace App\Exports\Report;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class LaporanStock implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function headings(): array
    {
        return [
            'No',
            'Nama Bahan',
            'Tipe Bahan',
            'Kode Bahan',
            'Satuan',
            'Dipakai',
            'Tersedia',
            'Invoice Pakai',
            'Tanggal Pembelian',
            'Terakhir Digunakan'
        ];
    }

    public function collection()
    {
        if(count($this->data) > 0){
            $data = [];
            foreach($this->data as $key => $val){
                $data[] = [
                    $key+1,
                    strtoupper($val->nama_bahan),
                    strtoupper($val->bahan_type),
                    strtoupper($val->code_bahan),
                    strtoupper($val->uom),
                    $val->qty_used,
                    $val->qty_available,
                    $val->invoice_number,
                    Carbon::parse($val->tanggal_beli)->format('d M Y H:i:s'),
                    Carbon::parse($val->last_used)->format('d M Y H:i:s')
                ];
            }
            $data_excels = [];
            $data_excels = $data;
            $export = new Collection([
                $data_excels
            ]);
            return $export;
        }else{
            $data = [];
            $data_excels = $data;
            $export = new Collection([
                $data_excels
            ]);
            return $export;
        }
    }
}
