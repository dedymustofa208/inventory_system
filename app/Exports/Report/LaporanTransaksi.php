<?php

namespace App\Exports\Report;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class LaporanTransaksi implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function headings(): array
    {
        return [
            'No',
            'Nama Client',
            'Invoice Number',
            'Qty Order',
            'Nama Barang',
            'Size',
            'Proses',
            'Total Price',
            'Status',
            'Order Date',
            'Dibuat Oleh',
            'Cancel Date',
            'Cancel By'
        ];
    }

    public function collection()
    {
        if(count($this->data) > 0){
            $data = [];
            foreach($this->data as $key => $val){
                if($key > 0){
                    if($val->invoice_number == $this->data[$key-1]->invoice_number){
                        $price = '';
                        $qty = '';
                    }else{
                        $price  =$val->total_price;
                        $qty  =$val->qty;
                    }
                }else{
                    $price = $val->total_price;
                    $qty = $val->qty;
                }
                $data[] = [
                    $key+1,
                    strtoupper($val->client_order),
                    $val->invoice_number,
                    $qty,
                    strtoupper($val->nama_barang),
                    strtoupper($val->size),
                    strtoupper($val->process_name),
                    $price,
                    $val->status,
                    Carbon::parse($val->order_date)->format('d M Y H:i:s'),
                    $val->created_by,
                    Carbon::parse($val->cancel_date)->format('d M Y H:i:s'),
                    $val->cancel_by
                ];
            }
            $data_excels = [];
            $data_excels = $data;
            $export = new Collection([
                $data_excels
            ]);
            return $export;
        }else{
            $data = [];
            $data_excels = $data;
            $export = new Collection([
                $data_excels
            ]);
            return $export;
        }
    }
}
