<div class="modal fade" id="modal_edit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Process</h4>
            <button type="button" class="close btn_close_mod" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card card-dark">
                <div class="card-header">
                    <h3 class="card-title">Edit Process</h3>
                </div>
                <form id="form_update" action="{{ route('process.update_data') }}" enctype="multipart/form-data" method="post">
                    <input type="hidden" class="form-controll id" name="id" id="id" />
                    <div class="card-body">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-indent"></i></span>
                                    </div>
                                    <input type="text" class="form-control nama_process text-uppercase" autocomplete="off" placeholder="Nama Process" id="nama_process" name="nama_process" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                    </div>
                                    <input type="text" class="form-control price" autocomplete="off" onkeypress="return numberOnly(event);" placeholder="Rp.- Harga" id="price" name="price" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="button" class="btn btn-primary w-100" id="button_update"><i class="fas fa-save"></i>&nbsp;Update</button>
                            </div>                                
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default btn_close_mod" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>