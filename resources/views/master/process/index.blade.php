@extends('layouts.app',['active' => 'process'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content-header">
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header">
                        <h5 class="m-0">Tambah Process Baru</h5>
                    </div>
                    <form id="form_add" action="{{ route('process.store') }}" enctype="multipart/form-data" method="post">
                        <div class="card-body">
                            @csrf
                            <div class="form-group row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-indent"></i></span>
                                        </div>
                                        <input type="text" class="form-control nama_process text-uppercase" autocomplete="off" placeholder="Nama Process" id="nama_process" name="nama_process" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" class="form-control price" autocomplete="off" onkeypress="return numberOnly(event);" placeholder="Rp.- Harga" id="price" name="price" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="button" class="btn btn-success w-100" id="button_add"><i class="fas fa-save"></i>&nbsp;Save</button>
                                </div>                                
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Table Process</h3>
                        </div>
                        <div class="card-body">
                        <table id="table-list" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nama Process</th>                                    
                                    <th>Harga</th>      
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-modal')
    @include('master.process.modal_edit')
@endsection

@section('page-js')
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    var table = $("#table-list").DataTable({
        ajax: {
            type: "GET",
            url: "{{ route('process.data') }}",
        },
        columnDefs: [                
            {
                targets : 2,
                width   : '25%'                 
            }
        ],
        columns: [
            {data: 'description', name: 'description',searchable:true,visible:true,orderable:false},            
            {data: 'price', name: 'price',searchable:true,visible:true,orderable:false},            
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ]
    });

    $('#button_add').on('click',function(){
        event.preventDefault();
        var name        = $('#nama_process').val();        
        var reg         = new RegExp(/[,()!~%`#$^&*=+;:'"<>|?]/g);
        if(reg.test(name)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#nama_process').val('');
            return false;
        }        
        var formData = new FormData(document.getElementById("form_add"));
        // var parameter = ['email_user','gender','username','password','photo_user','role'];
        // var val = [
        //     $('#email_user').val(),
        //     $('#gender').val(),
        //     $('#username').val(),
        //     $('#password').val(),
        //     $('#photo_user').val(),
        //     $('#role').val(),
        // ];
        // for(var i = 0; i < parameter.length; i++ ) {
        //     formData.append(parameter[i], val[i]);
        // };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : $('#form_add').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#form_add').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px'
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $('#form_add').unblock();
                $('#form_add').trigger("reset");                
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $('#form_add').unblock();
                Swal.fire({
                    title:'Oopps...!', 
                    text:'Something Went Wrong!',
                    icon:'error',
                    timer:1000
                });
            }
        });
    });

    $('#nama_process').blur(function(){
    var nama_process = $(this).val();
        if (nama_process == '') {
            $('.nama_process').addClass('is-invalid');
        } else {
            $('.nama_process').removeClass('is-invalid');
        }
    });


    $('#table-list').on('click','.edit', function(){
        let name = $(this).data('name');
        let id = $(this).data('id');
        let price = $(this).data('price');

        $('#form_update .id').val(id);
        $('#form_update .nama_process').val(name);
        $('#form_update .price').val(price);
        $('#modal_edit').modal('show');
    });

    $('#button_update').on('click',function(event){
        event.preventDefault();
        var name        = $('#form_update .nama_process').val(); 
        var price       = $('#form_update .price').val();
        var id          = $('#form_update .id').val(); 
        var reg         = new RegExp(/[,()!~%`#$^&*=+;:'"<>|?]/g);
        if(reg.test(name)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#form_add .nama_process').val('');
            return false;
        }        
        var formData = new FormData(document.getElementById("form_update"));
        // var parameter = ['email_user','gender','username','password','photo_user','role'];
        // var val = [
        //     $('#email_user').val(),
        //     $('#gender').val(),
        //     $('#username').val(),
        //     $('#password').val(),
        //     $('#photo_user').val(),
        //     $('#role').val(),
        // ];
        // for(var i = 0; i < parameter.length; i++ ) {
        //     formData.append(parameter[i], val[i]);
        // };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : $('#form_update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#form_update').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px'
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $('#form_update').unblock();
                $('#form_update').trigger("reset");
                $('#modal_edit').modal('hide');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $('#form_update').unblock();
                Swal.fire({
                    title:'Oopps...!', 
                    text:response.responseJSON.toString(),
                    icon:'error'
                });
            }
        });
    });

    $('#table-list').on('click','.delete', function(){
        let name = $(this).data('name');
        let id = $(this).data('id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : "{{ route('process.delete_data') }}",
            data: {id, name},                    
            beforeSend: function () {
                $('#form_update').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px'
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $('#form_update').unblock();
                $('#form_update').trigger("reset");
                $('#modal_edit').modal('hide');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $('#form_update').unblock();
                Swal.fire({
                    title:'Oopps...!', 
                    text:'Something Went Wrong!',
                    icon:'error',
                    timer:1000
                });
            }
        });
    });

});
</script>
@endsection

