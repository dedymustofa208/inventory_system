@extends('layouts.app',['active' => 'master_bahan_baku'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Table Bahan Baku</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12 add_barang">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                                </div>
                                <input type="text" class="form-control nama_bahan text-uppercase" style="width:10%" autocomplete="off" placeholder="Nama Bahan" id="nama_bahan" name="nama_bahan" required></input>
                                {{-- <input type="text" class="form-control code_bahan text-uppercase" style="width:10%" autocomplete="off" placeholder="Kode Bahan" id="code_bahan" name="code_bahan" required></input> --}}
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                                </div>
                                <select class="form-control select warna" id="type_warna" name="type_warna" data-placeholder="Pilih Warna" data-allow-clear="true">
                                    <option value="" disabled selected></option>
                                    @foreach($warna as $key => $val)
                                        <option value="{{ $key }}">{{ strtoupper($val) }}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                </div>
                                <input type="text" class="form-control nama_bahan text-uppercase" title="Harga Barang" style="width:10%" onkeypress="return numberOnly(event);" autocomplete="off" placeholder="Rp. 0" id="harga_barang" name="harga_barang" required></input>
                                <select class="form-control select uoms" id="uoms" name="uoms" data-placeholder="Pilih Satuan" data-allow-clear="true">
                                    <option value="" disabled selected></option>
                                    @foreach($uom as $key => $val)
                                        <option value="{{ $key }}">{{ strtoupper($val) }}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                                </div>
                                <select class="form-control select bahan_type" id="bahan_type" name="bahan_type" data-placeholder="Pilih Type" data-allow-clear="true">
                                    <option value="" disabled selected></option>
                                    @foreach($type as $key => $val)
                                        <option value="{{ $key }}">{{ strtoupper($val) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="button" class="btn btn-success" id="button_add"><i class="fas fa-save"></i>&nbsp;Save</button>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <table id="table-list" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nama Bahan</th>
                                    <th>Kode</th>
                                    <th>Warna</th>
                                    <th>Harga Bahan</th>
                                    <th>Satuan</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-modal')
    @include('master.bahan_baku.modal_edit')
@endsection
@section('page-js')
<script>
    function edit(url){
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $('#table-list').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'20px',
                },message:'Please Wait...'});
            },
            success: function (response) {
                $('#table-list').unblock();
                var description = response.description;
                var harga_bahan = response.unit_prices;
                var id = response.id
                $('#id_update').val(id);
                $('#nama_bahan_update').val(description);
                $('#uoms_update').val(response.uom).change();
                $('#harga_barang_update').val(harga_bahan);
                $('#type_warna_update').val(response.color_name).change();
                $('#bahan_type_update').val(response.bahan_type).change();
                $('#modal_edit').modal('show');
            }
        });
    }
    function deletes(id){
        $.ajax({
            type: "get",
            url: "/master/delete-bahan-baku/"+id,
            beforeSend: function () {
                $('#table-list').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'20px',
                },message: 'Please Wait...'
            }); 
            },
            success: function (response) {
                $('#table-list').unblock();
                Swal.fire({
                    title:'Warning..!', 
                    text:response.toString(),
                    icon:'warning'
                });
                $("#table-list").DataTable().ajax.reload();
            },
            error: function(response){
                
                $('#table-list').unblock();
                if(response.status == 422){
                    Swal.fire({
                        title:'Oopps...!', 
                        text:response.responseJSON.toString(),
                        icon:'error'
                    });
                }else{
                    Swal.fire({
                        title:'Oopps...!', 
                        text:response.responseJSON,
                        icon:'error',
                        timer:1000
                    });
                }
            }
        })
    }
</script>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $('#bahan_type, #uoms').on('change', function (e) {
        e.preventDefault();
        const selectedBahan = $('#bahan_type').val();
        const selectedUom = $('#uoms').val();
        const bahanType = {
            'benang': {
                'based_uom': ['m', 'cm', 'cones']
            },
            'cat': {
                'based_uom': ['kg', 'liter', 'ml']
            },
            'alcohol': {
                'based_uom': ['kg', 'liter', 'ml']
            },
            'mica': {
                'based_uom': ['pcs']
            }
        };
        if (selectedBahan && bahanType[selectedBahan] && !bahanType[selectedBahan].based_uom.includes(selectedUom)) {
            Swal.fire({
                title: 'Oopps...!',
                text: 'Satuan Tidak Sesuai',
                icon: 'error'
            });      
            $('#bahan_type').val('').trigger('change'); 
        }
    });


    $('.select').select2({
        theme: 'bootstrap4'
    });
    $(function () {
        bsCustomFileInput.init();
    });
    
    $("#table-list").on("click", ".edit", function() {
        var id = $(this).data('id');
        var url = "/master/edit-bahan-baku"+"/"+id;
        edit(url);
    });
    $("#table-list").on("click", ".deletes", function() {
        var id = $(this).data('id');
        deletes(id);
    });
    
    $.extend( $.fn.dataTable.defaults, {
        responsive: false, 
        autoWidth: true,
        autoLength:true,
        serverSide: true,
        deferRender: true,
        info:true,
        bFilter:true,
        lengthChange: true,
    });

    var table = $("#table-list").DataTable({
        ajax: {
            type: "GET",
            url: "{{ route('bahan_baku.dataBahanBaku') }}",
        },
        columns: [
            {data: 'description', name: 'description'},
            {data: 'code_bahan', name: 'code_bahan'},
            {data: 'color_name', name: 'color_name'},
            {data: 'unit_prices', name: 'unit_prices'},
            {data: 'uom', name: 'uom'},
            {data: 'bahan_type', name: 'bahan_type'},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ]
    });

    $('#button_update').on('click',function(event){
        event.preventDefault();
        var id_update = $('#id_update').val();
        var nama_bahan = $('#nama_bahan_update').val();
        var type_warna = $('#type_warna_update').val();
        var uoms = $('#uoms_update').val();
        var harga_barang = $('#harga_barang_update').val();
        var bahan_type = $('#bahan_type_update').val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : "{{ route('bahan_baku.updateBahanBaku') }}",
            data: {
                id_update:id_update,
                nama_bahan:nama_bahan,
                type_warna:type_warna,
                bahan_type:bahan_type,
                uoms:uoms,
                harga_barang:harga_barang
            },
            beforeSend: function () {
                $('#modal_edit').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px',
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $('#modal_edit').unblock();
                $('#modal_edit').modal('hide');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $('#modal_edit').unblock();
                Swal.fire({
                    title:'Oopps...!', 
                    text:response.responseJSON,
                    icon:'error'
                });
                $('#modal_edit').modal('show');
            }
        });
    });

    $('#button_add').on('click',function(){
        event.preventDefault();
        var nama_bahan = $('#nama_bahan').val();
        var type_warna = $('#type_warna').val();
        var bahan_type = $('#bahan_type').val();
        var harga_barang = $('#harga_barang').val();
        var uoms = $('#uoms').val();
        var reg = new RegExp(/[,()!~%`#$^&*=+;:'"<>|?]/g);
        if(reg.test(nama_bahan)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#nama_bahan').val('');
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : "{{ route('bahan_baku.addBahanBaku') }}",
            data: {
                nama_bahan:nama_bahan,
                type_warna:type_warna,
                bahan_type:bahan_type,
                harga_barang:harga_barang,
                uoms:uoms
            },
            beforeSend: function () {
                $.blockUI({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px'
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $.unblockUI();
                $('#nama_bahan').val('').focus();
                $('#harga_barang').val('');
                $('#type_warna').val('').change();
                $('#bahan_type').val('').change();
                $('#uoms').val('').change();
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $.unblockUI();
                Swal.fire({
                    title:'Oopps...!', 
                    text:response.responseJSON,
                    icon:'error'
                });
            }
        });
    });

    $('#nama_bahan').blur(function(){
        var nama_bahan = $(this).val();
        if (nama_bahan == '') {
            $('.nama_bahan').addClass('is-invalid');
        } else {
            $('.nama_bahan').removeClass('is-invalid');
        }
    });
});
</script>
@endsection