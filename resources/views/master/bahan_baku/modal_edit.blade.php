<div class="modal fade" id="modal_edit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Nama Barang</h4>
            <button type="button" class="close btn_close_mod" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="id_update" class="form-control" />
            <div class="col-md-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-box"></i></span>
                    </div>
                    <input type="text" class="form-control nama_bahan_update text-uppercase" style="width:10%" autocomplete="off" placeholder="Nama Bahan" id="nama_bahan_update" name="nama_bahan_update" required></input>
                    {{-- <input type="text" class="form-control code_bahan_update text-uppercase" style="width:10%" autocomplete="off" placeholder="Kode Bahan" id="code_bahan_update" name="code_bahan_update" required></input> --}}
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-box"></i></span>
                    </div>
                    <select class="form-control select warna" id="type_warna_update" name="type_warna_update" data-placeholder="Pilih Warna" data-allow-clear="true">
                        <option value="" disabled selected></option>
                        @foreach($warna as $key => $val)
                            <option value="{{ $key }}">{{ strtoupper($val) }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-box"></i></span>
                    </div>
                    <select class="form-control select bahan_type" id="bahan_type_update" name="bahan_type_update" data-placeholder="Pilih Type" data-allow-clear="true">
                        <option value="" disabled selected></option>
                        @foreach($type as $key => $val)
                            <option value="{{ $key }}">{{ strtoupper($val) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                    </div>
                    <input type="text" class="form-control nama_bahan text-uppercase" title="Harga Barang" onkeypress="return numberOnly(event);" style="width:15%" autocomplete="off" placeholder="Rp. 0" id="harga_barang_update" name="harga_barang_update" required></input>
                    <select class="form-control select uoms" id="uoms_update" name="uoms_update" data-placeholder="Pilih Satuan" data-allow-clear="true">
                        <option value="" disabled selected></option>
                        @foreach($uom as $key => $val)
                            <option value="{{ $key }}">{{ strtoupper($val) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default btn_close_mod" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="button_update"><i class="fas fa-save"></i>&nbsp;Save</button>
        </div>
        </div>
    </div>
</div>