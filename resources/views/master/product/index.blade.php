@extends('layouts.app',['active' => 'master_product'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header">
                        <h5>Tambah Produk Baru</h5>
                    </div>
                    <form id="form_add" action="{{ route('product.addProduct') }}" enctype="multipart/form-data" method="post">
                        <div class="card-body">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-box"></i></span>
                                        </div>
                                        <select class="form-control select barang" id="barang" name="barang" data-placeholder="Pilih Barang" data-allow-clear="true">
                                            <option value="" disabled selected></option>
                                            @foreach($barang as $b)
                                                <option value="{{ $b->id }}">{{ strtoupper($b->description) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-list"></i></span>
                                        </div>
                                        <select class="form-control select process" id="process" name="process[]" multiple data-placeholder="Pilih Process" data-allow-clear="true">
                                            <option value=""></option>
                                            @foreach($process as $p)
                                            <option value="{{ $p->id }}">{{ strtoupper($p->description) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-list"></i></span>
                                        </div>
                                        <select class="form-control select size" id="size" name="size[]" multiple data-placeholder="Pilih Ukuran" data-allow-clear="true">
                                            @foreach($size as $s)
                                            <option value="{{ $s->description }}">{{ strtoupper($s->description) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="photo_product" name="photo_product" accept="image/*">
                                            <label class="custom-file-label">Photo Produk</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 p-0 m-0">
                                    <div class="col-md-6 m-0 p-0">
                                        <div class="alert alert-info alert-dismissible mt-3 mx-2 pb-1 pt-1">
                                            <button type="button" class="close pt-1" data-dismiss="alert" aria-hidden="true">×</button>
                                            <label class="m-0 p-0"><i class="icon fas fa-info"></i> Daftar Bahan Baku Utama!</label>
                                        </div>
                                    </div>
                                    <div id="repeater">
                                        <div class="input-group entry mt-3">
                                            <div class="col-md-2">
                                                <select class="form-control selects" id="tipeBahan" name="tipeBahan[]" data-placeholder="Pilih Bahan" data-allow-clear="true">
                                                    <option value=""></option>
                                                    @foreach($bahan_type as $key => $val)
                                                        <option value="{{ $key }}">{{ strtoupper($val) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <select class="form-control selects" id="uom" name="uom[]" data-placeholder="Pilih Satuan" data-allow-clear="true">
                                                    <option value=""></option>
                                                    @foreach($uom as $key => $val)
                                                        <option value="{{ $key }}">{{ strtoupper($val) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="number" name="qty[]" id="qty" class="form-control" placeholder="Jumlah">
                                            </div>
                                            <button type="button" class="btn btn-success btn-add" id="plus-elem"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-dark">
                            <button type="button" class="btn btn-success" id="button_add"><i class="fas fa-save"></i>&nbsp;Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Table Produk</h3>
                        </div>
                        <div class="card-body">
                        <table id="table-list" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:25%">Nama Barang</th>
                                    <th style="width:10%">Proses</th>
                                    <th style="width:5%">Size</th>
                                    <th style="width:20%">Price</th>
                                    <th style="width:20%; text-align:center">Image</th>
                                    <th style="width:10%; text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-modal')
    {{--@include('admin.roles.modal_edit')--}}
@endsection
@section('page-js')
<script type="text/javascript">
    function deletes(id){
        $.ajax({
            type: "get",
            url: "/master/delete-product/"+id,
            beforeSend: function () {
                $('#table-list').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'20px',
                },message: 'Please Wait...'
            }); 
            },
            success: function (response) {
                $('#table-list').unblock();
                Swal.fire({
                    title:'Warning..!', 
                    text:response.toString(),
                    icon:'warning'
                });
                $("#table-list").DataTable().ajax.reload();
            },
            error: function(response){
                
                $('#table-list').unblock();
                if(response.status == 422){
                    Swal.fire({
                        title:'Warning..!', 
                        text:response.responseJSON.toString(),
                        icon:'warning'
                    });
                }else{
                    Swal.fire({
                        title:'Oopps...!', 
                        text:'Something Went Wrong!',
                        icon:'error',
                        timer:1000
                    });
                }
            }
        })
    }
</script>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $("#repeater").on("click", ".btn-add", function () {
        let entry = $(this).closest(".entry").clone();

        // Hitung jumlah entry yang sudah ada untuk mendapatkan index baru
        let index = $("#repeater .entry").length;

        // Update atribut ID dan Name dengan index unik
        entry.find("select").each(function () {
            let newId = $(this).attr("id").split("_")[0] + "_" + index;
            let newName = $(this).attr("name").replace(/\[\d*\]/, `[${index}]`);
            $(this).attr("id", newId);
            $(this).attr("name", newName);
            $(this).removeClass("select").addClass("selects"); // Ubah class dari select ke selects
            $(this).val(null); // Reset value
        });

        entry.find("input").each(function () {
            let newId = $(this).attr("id").split("_")[0] + "_" + index;
            let newName = $(this).attr("name").replace(/\[\d*\]/, `[${index}]`);
            $(this).attr("id", newId);
            $(this).attr("name", newName);
            $(this).val(""); // Reset value
        });

        // Ubah tombol tambah menjadi tombol hapus
        entry.find(".btn-add")
            .removeClass("btn-success btn-add")
            .addClass("btn-danger btn-remove")
            .html('<i class="fa fa-minus"></i>');

        $("#repeater").append(entry);

        // Reinitialize Select2 hanya untuk elemen baru yang menggunakan .selects
        entry.find('.selects').select2({
            theme: 'bootstrap4'
        });
    });

    // Event untuk menghapus entry
    $("#repeater").on("click", ".btn-remove", function () {
        $(this).closest(".entry").remove();
        console.log($(this).closest(".entry").find("select").attr("id"));
    });

    $('#tipeBahan, #uom').on('change', function (e) {
        e.preventDefault();
        const selectedBahan = $('#tipeBahan').val();
        const selectedUom = $('#uom').val();
        const bahanType = {
            'benang': {
                'based_uom': ['m', 'cm', 'cones']
            },
            'cat': {
                'based_uom': ['kg', 'liter', 'ml']
            },
            'alcohol': {
                'based_uom': ['kg', 'liter', 'ml']
            },
            'mica': {
                'based_uom': ['pcs']
            }
        };
        if (selectedBahan && bahanType[selectedBahan] && selectedUom !== '' && !bahanType[selectedBahan].based_uom.includes(selectedUom)) {
            Swal.fire({
                title: 'Oopps...!',
                text: 'Satuan Tidak Sesuai',
                icon: 'error'
            });      
            $('#uom').val('').trigger('change'); 
        }
    });

    $('.select').select2({
        theme: 'bootstrap4'
    });
    $(function () {
        bsCustomFileInput.init();
    });

    $("#table-list").on("click", ".edit", function() {
        var id = $(this).data('id');
        var url = "/master/edit-product"+"/"+id;
        edit(url);
    });
    $("#table-list").on("click", ".deletes", function() {
        var id = $(this).data('id');
        deletes(id);
    });

    $.extend( $.fn.dataTable.defaults, {
        responsive: false, 
        autoWidth: true,
        autoLength:true,
        serverSide:true,
        info:true,
        bFilter:true,
        lengthChange: true,
    });

    var table = $("#table-list").DataTable({
        ajax: {
            type: "GET",
            url: "{{ route('product.dataProduct') }}",
        },
        columnDefs: [                
            {
                targets : 4,
                css   : 'text-center'                 
            }
        ],
        columns: [
            {data: 'barang_id', name: 'barang_id',searchable:true,visible:true,orderable:false},
            {data: 'process_id', name: 'process_id',searchable:true,visible:true,orderable:false},
            {data: 'size', name: 'size',searchable:true,visible:true,orderable:false},
            {data: 'total_price', name: 'total_price',searchable:true,visible:true,orderable:false},
            {data: 'photo_product', name: 'photo_product',searchable:true,visible:true,orderable:false},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ]
    });

    $('#button_add').on('click',function(){
        event.preventDefault();
        var formData = new FormData(document.getElementById("form_add"));
        var parameter = ['barang','process','size','photo_product'];
        var val = [
            $('#barang').val(),
            $('#process').val(),
            $('#size').val(),
            $('#photo_product').val()
        ];
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : $('#form_add').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px'
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $.unblockUI();
                $('#barang').val('').change();
                $('#process').val('').change();
                $('#size').val('').change();
                $('#photo_product').val('');
                $('.custom-file-label').text('');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
                setTimeout(() => {
                    location.reload();
                }, 200);
            },
            error: function(response){
                $.unblockUI();
                if(response.status === 422){
                    Swal.fire({
                        title:'Oopps...!', 
                        text:response.responseJSON,
                        icon:'error',
                        timer:1000
                    });
                }else{
                    Swal.fire({
                        title:'Oopps...!', 
                        text:'Something Went Wrong!',
                        icon:'error',
                        timer:1000
                    });
                }
            }
        });
    });

    $('input[name="email_user"]').blur(function(){
    var emailValidator = /[@]/g;
    var email = $(this).val();
        if (!emailValidator.test(email)) {
            $('.email').addClass('is-invalid');
        } else {
            $('.email').removeClass('is-invalid');
        }
    });

    $('#username').blur(function(){
    var username = $(this).val();
        if (username == '') {
            $('.username').addClass('is-invalid');
        } else {
            $('.username').removeClass('is-invalid');
        }
    });

    $('#password').blur(function(){
    var password = $(this).val();
        if (password == '') {
            $('.password').addClass('is-invalid');
        } else {
            $('.password').removeClass('is-invalid');
        }
    });

    $('#button_update').on('click',function(){
        
        var name        = $('#name_update').val();
        var email       = $('#email_update').val();
        var gender      = $('#gender_update').val();
        var password    = $('#password_update').val();
        
        var role        = $('#role_update').val();
        var id_update = $('#id_update').val();
        var reg = new RegExp(/[,()!~%`#$^&*=+;:'"<>|?]/g);
        
        if(reg.test(name)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#name_update').val('');
            return false;
        }
        if(reg.test(email)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#email_update').val('');
            return false;
        }
        
        event.preventDefault();
        var formData = new FormData(document.getElementById("form_update"));
        var parameter = ['id_update','permission_update'];
        var val = [
            $('#id_update').val(),
            $('#permission_update').val()
        ];
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : $('#form_update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#modal_edit').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px',
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $('#modal_edit').unblock();
                $('#modal_edit').modal('hide');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $('#modal_edit').unblock();
                Swal.fire({
                    title:'Oopps...!', 
                    text:'Something Went Wrong!',
                    icon:'error',
                    timer:1000
                });
            }
        });
    });
});
</script>
@endsection