@extends('layouts.app',['active' => 'master_supplier'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Table Supplier</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12 add_barang">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                                </div>
                                <input type="text" class="form-control nama_supplier text-uppercase" autocomplete="off" placeholder="Nama Supplier" id="nama_supplier" name="nama_supplier" required>
                                <button type="button" class="btn btn-success pl-2" id="button_add"><i class="fas fa-save"></i>&nbsp;Save</button>
                            </div>
                        </div>
                        <hr>
                        <table id="table-list" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:75%">Nama Supplier</th>
                                    <th style="width:25%">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-modal')
    @include('master.supplier.modal_edit')
@endsection
@section('page-js')
<script>
    function edit(url){
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $('#table-list').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'20px',
                },message:'Please Wait...'});
            },
            success: function (response) {
                $('#table-list').unblock();
                var description = response.description;
                var id = response.id

                $('#id_update').val(id);
                $('#nama_supplier_update').val(description);
                $('#modal_edit').modal('show');
            }
        });
    }
    function deletes(id){
        $.ajax({
            type: "get",
            url: "/master/delete-supplier/"+id,
            beforeSend: function () {
                $('#table-list').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'20px',
                },message: 'Please Wait...'
            }); 
            },
            success: function (response) {
                $('#table-list').unblock();
                Swal.fire({
                    title:'Warning', 
                    text:response.toString(),
                    icon:'warning'
                });
                $("#table-list").DataTable().ajax.reload();
            },
            error: function(response){
                $('#table-list').unblock();
                if(response.status == 422){
                    Swal.fire({
                        title:'Oopps...!', 
                        text:response.responseJSON.toString(),
                        icon:'error'
                    });
                }else{
                    Swal.fire({
                        title:'Oopps...!', 
                        text:'Something Went Wrong!',
                        icon:'error',
                        timer:1000
                    });
                }
            }
        })
    }
</script>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $("#table-list").on("click", ".edit", function() {
        var id = $(this).data('id');
        var url = "/master/edit-supplier"+"/"+id;
        edit(url);
    });
    $("#table-list").on("click", ".deletes", function() {
        var id = $(this).data('id');
        deletes(id);
    });
    
    $.extend( $.fn.dataTable.defaults, {
        responsive: false, 
        autoWidth: true,
        autoLength:true,
        info:true,
        bFilter:true,
        lengthChange: true,
    });

    var table = $("#table-list").DataTable({
        ajax: {
            type: "GET",
            url: "{{ route('supplier.dataSupplier') }}",
        },
        columns: [
            {data: 'description', name: 'description',searchable:true,visible:true,orderable:true},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ]
    });

    $('#button_add').on('click',function(){
        event.preventDefault();
        var nama_supplier = $('#nama_supplier').val();
        var reg = new RegExp(/[,()!~%`#$^&*=+;:'"<>|?]/g);
        if(reg.test(nama_supplier)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#nama_supplier').val('');
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : "{{ route('supplier.addSupplier') }}",
            data: {
                nama_supplier:nama_supplier
            },
            beforeSend: function () {
                $.blockUI({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px'
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $.unblockUI();
                $('#nama_supplier').val('').focus();
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $.unblockUI();
                Swal.fire({
                    title:'Oopps...!', 
                    text:response.responseJSON,
                    icon:'error'
                });
            }
        });
    });

    $('#nama_supplier').blur(function(){
    var nama_supplier = $(this).val();
        if (nama_supplier == '') {
            $('.nama_supplier').addClass('is-invalid');
        } else {
            $('.nama_supplier').removeClass('is-invalid');
        }
    });

    $('#button_update').on('click',function(){
        var nama_supplier_update = $('#nama_supplier_update').val();
        var id_update   = $('#id_update').val();
        var reg         = new RegExp(/[,()!~%`#$^&*=+;:'"<>|?]/g);
        
        if(reg.test(nama_supplier_update)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#nama_supplier_update').val('').focus();
            return false;
        }        
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : "{{ route('supplier.updateSupplier') }}",
            data: {
                id_update:id_update,
                nama_supplier_update:nama_supplier_update
            },
            beforeSend: function () {
                $('#modal_edit').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px',
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $('#modal_edit').unblock();
                $('#modal_edit').modal('hide');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $('#modal_edit').unblock();
                Swal.fire({
                    title:'Oopps...!', 
                    text:response.responseJSON,
                    icon:'error'
                });
                $('#modal_edit').modal('show');
                $('#nama_supplier_update').focus();
            }
        });
    });
});
</script>
@endsection