@extends('layouts.app',['active' => 'master_size'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Table Barang</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12 add_barang">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-ruler"></i></span>
                                </div>
                                <input type="text" class="form-control size text-uppercase mr-3" autocomplete="off" placeholder="Size" id="size" name="size" required>
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                </div>
                                <input type="text" class="form-control additional_price mr-3" autocomplete="off" onkeypress="return numberOnly(event);" placeholder="Rp.- (Additional Price)" id="additional_price" name="additional_price" required>
                                <button type="button" class="btn btn-success ml-2" id="button_add"><i class="fas fa-save"></i>&nbsp;Save</button>
                            </div>
                        </div>
                        <hr>
                        <table id="table-list" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:40%">Size</th>
                                    <th style="width:30%">Additional Price</th>
                                    <th style="width:30%">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-modal')
    @include('master.size.modal_edit')
@endsection
@section('page-js')
<script>
    function edit(url){
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $('#table-list').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'20px',
                },message:'Please Wait...'});
            },
            success: function (response) {
                $('#table-list').unblock();
                var description = response.description;
                var additional_price = response.additional_price;
                var id = response.id

                $('#id_update').val(id);
                $('#size_update').val(description);
                $('#additional_price_update').val(additional_price);
                $('#modal_edit').modal('show');
            }
        });
    }
    function deletes(id){
        $.ajax({
            type: "get",
            url: "/master/delete-size/"+id,
            beforeSend: function () {
                $('#table-list').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'20px',
                },message: 'Please Wait...'
            }); 
            },
            success: function (response) {
                $('#table-list').unblock();
                Swal.fire({
                    title:'Warning..!', 
                    text:response.toString(),
                    icon:'warning'
                });
                $("#table-list").DataTable().ajax.reload();
            },
            error: function(response){
                
                $('#table-list').unblock();
                if(response.status == 422){
                    Swal.fire({
                        title:'Oopps...!', 
                        text:response.responseJSON.toString(),
                        icon:'error'
                    });
                }else{
                    Swal.fire({
                        title:'Oopps...!', 
                        text:'Something Went Wrong!',
                        icon:'error',
                        timer:1000
                    });
                }
            }
        })
    }
</script>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $("#table-list").on("click", ".edit", function() {
        var id = $(this).data('id');
        var url = "/master/edit-size"+"/"+id;
        edit(url);
    });
    $("#table-list").on("click", ".deletes", function() {
        var id = $(this).data('id');
        deletes(id);
    });
    
    $.extend( $.fn.dataTable.defaults, {
        responsive: false, 
        autoWidth: true,
        autoLength:true,
        info:true,
        bFilter:true,
        lengthChange: true,
    });

    var table = $("#table-list").DataTable({
        ajax: {
            type: "GET",
            url: "{{ route('size.dataSize') }}",
        },
        columns: [
            {data: 'description', name: 'description',searchable:true,visible:true,orderable:false},
            {data: 'additional_price', name: 'additional_price',searchable:true,visible:true,orderable:false},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ]
    });

    $('#button_add').on('click',function(){
        event.preventDefault();
        var size = $('#size').val();
        var additional_price = $('#additional_price').val();
        var reg = new RegExp(/[,()!~%`#$^&*=+;:'"<>|?]/g);
        if(reg.test(size)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#size').val('');
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : "{{ route('size.addSize') }}",
            data: {
                size:size,
                additional_price:additional_price
            },
            beforeSend: function () {
                $.blockUI({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px'
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $.unblockUI();
                $('#size').focus();
                $('#additional_price').val('');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $.unblockUI();
                Swal.fire({
                    title:'Oopps...!', 
                    text:response.responseJSON,
                    icon:'error'
                });
            }
        });
    });

    $('#size').blur(function(){
        var size = $(this).val();
        if (size == '') {
            $('.size').addClass('is-invalid');
        } else {
            $('.size').removeClass('is-invalid');
        }
    });

    $('#button_update').on('click',function(){
        var size_update = $('#size_update').val();
        var additional_price_update = $('#additional_price_update').val();
        var id_update   = $('#id_update').val();
        var reg         = new RegExp(/[,()!~%`#$^&*=+;:'"<>|?]/g);
        
        if(reg.test(size_update)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#size_update').val('').focus();
            return false;
        }        
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : "{{ route('size.updateSize') }}",
            data: {
                id_update:id_update,
                size_update:size_update,
                additional_price_update:additional_price_update
            },
            beforeSend: function () {
                $.blockUI({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px',
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $.unblockUI();
                $('#modal_edit').modal('hide');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $.unblockUI();
                Swal.fire({
                    title:'Oopps...!', 
                    text:response.responseJSON,
                    icon:'error'
                });
                $('#modal_edit').modal('show');
                $('#nama_barang_update').focus();
            }
        });
    });
});
</script>
@endsection