<div class="modal fade" id="modal_edit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Nama Barang</h4>
            <button type="button" class="close btn_close_mod" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="id_update" class="form-control" />
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                </div>
                <input type="text" class="form-control nama_barang_update" autocomplete="off" placeholder="Nama Barang" id="nama_barang_update" name="nama_barang_update" required>
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                </div>
                <input type="text" class="form-control price mr-3" autocomplete="off" onkeypress="return numberOnly(event);" placeholder="Rp.- Harga" id="harga_barang_update" name="harga_barang_update" required>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default btn_close_mod" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="button_update"><i class="fas fa-save"></i>&nbsp;Save</button>
        </div>
        </div>
    </div>
</div>