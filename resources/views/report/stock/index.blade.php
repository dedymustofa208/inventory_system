@extends('layouts.app',['active' => 'laporan_stock'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="card-body">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Laporan Stock Bahan Baku</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="form-group">
                                <div class="input-group">
                                    <button type="button" target="_blank" class="btn btn-success ml-2" id="btn_download"><i class="fas fa-download"></i>&nbsp;DOWNLOAD</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-js')
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $('#order_date').daterangepicker({
        locale: {
            format: 'DD MMMM YYYY'
        },
        startDate: moment(),
        endDate  : moment().add(7, 'days'),
    });

    $('#btn_download').click(function(){
        var url = "{{ route('report.downloadStockBahan') }}";
        window.open(url, '_blank');
    });
});
</script>
@endsection