@extends('layouts.app',['active' => 'laporan_transaksi'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="card-body">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Laporan Transaksi</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="form-group">
                                <label>Order Date:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control float-right" id="order_date">
                                    <button type="button" target="_blank" class="btn btn-success ml-2" id="btn_download"><i class="fas fa-download"></i>&nbsp;DOWNLOAD</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-js')
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $('#order_date').daterangepicker({
        locale: {
            format: 'DD MMMM YYYY'
        },
        startDate: moment(),
        endDate  : moment().add(7, 'days'),
    });

    $('#btn_download').click(function(){
        var order_date = $('#order_date').val();
        var startDate = moment(order_date.split(' - ')[0], 'DD MMMM YYYY').format('YYYY-MM-DD 00:00:00');
        var endDate = moment(order_date.split(' - ')[1], 'DD MMMM YYYY').format('YYYY-MM-DD 23:59:59');
        var url = "{{ route('report.downloadTransaksi') }}?start_date=" + startDate+"&end_date="+endDate;
        window.open(url, '_blank');
    });
});
</script>
@endsection