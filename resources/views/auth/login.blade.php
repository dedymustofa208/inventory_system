<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LOGIN</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/adminlte.min.css') }}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
        <a href="#" class="h3"><b>INVENTORY|</b>SYSTEM</a>
        </div>
        <div class="card-body">
        <form  method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="input-group mb-3">
                <input type="text" class="form-control {{ $errors->has('email') ? 'has-error' : '' }}" value="{{ old('email') }}" name="email" id="email" placeholder="Email" required>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
                <div class="input-group-append">
                    <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            @if ($errors->has('email'))
            <span class="help-block text-danger text-center">
                <p class="fs-sm fw-semibold">{{ $errors->first('email') }}</p>
            </span>
            @elseif (session()->has('flash_notification.message'))
            <span class="help-block text-danger text-center">
                <p class="fs-sm fw-semibold">{!! session()->get('flash_notification.message') !!}</p>
            </span>
            @endif
            <div class="row">
            <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">LOGIN</button>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="{{ asset('js/admin_asset/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('js/admin_asset/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/admin_asset/adminlte.min.js') }}"></script>
</body>
</html>
