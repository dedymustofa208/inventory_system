@extends('layouts.app',['active' => 'dashboard'])

@section('content-header')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
                <h4 class="breadcrumb-item"><a href="#">Home</a></h4>
                <h4 class="breadcrumb-item active">Dashboard</h4>
            </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-content')
    @if(in_array('menu-dashboard',Auth::user()->getAllPermissions()->pluck('name')->toarray()) || Auth::user()->hasRole('superuser'))
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{ $barang }}</h3>
                                <p>Total Barang</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-box"></i>
                            </div>
                            <a href="{{ route('barang.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-pink">
                            <div class="inner">
                                <h3>{{ number_format($total_order) }}</h3>
                                <p>Total Order</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-business-time"></i>
                            </div>
                            <a href="{{ route('order.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{ $prod_print }}</h3>
                                <p>Product Printing</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-paint-roller"></i>
                            </div>
                            <a href="{{ route('product.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>{{ $prod_embro }}</h3>
                                <p>Product Embrodiery</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-wave-square"></i>
                            </div>
                            <a href="{{ route('product.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-teal">
                            <div class="inner">
                                <h3>{{ number_format($total_invoice) }}</h3>
                                <p>Total Invoice</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-file-alt"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-dark" style="opacity:0.78">
                            <div class="inner">
                                <h3>{{ number_format($cancel_order) }}</h3>
                                <p>Canceled Invoice</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-minus"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{ number_format($outstanding_invoice) }}</h3>
                                <p>Outstanding Invoice</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-file-alt"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-indigo" style="opacity:0.75">
                            <div class="inner">
                                <h3>{{ number_format($paid_invoice) }}</h3>
                                <p>Paid Invoice</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-file-alt"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>Rp. {{ number_format($total_jual) }}</h3>
                                <p>Total Penjualan</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-dollar-sign"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-info" style="opacity:0.75">
                            <div class="inner">
                                <h3>Rp. {{ number_format($total_rugi) }}</h3>
                                <p>TOTAL KERUGIAN</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-cube"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-dark">
                            <div class="card-header bg-dark">
                                <h3 class="card-title">Waiting List Order</h3>
                            </div>
                            <div class="card-body">
                                <table id="table-list" class="table table-bordered table-striped" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Invoice No</th>
                                            <th>Nama Client</th>
                                            <th>Status</th>
                                            <th>Tanggal Order</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @else
        <section class="content">
            <div class="container-fluid">
                <div class="text-center">
                    <img src="{{ asset('admin_asset/img/invasys_logo2.png') }}" alt="Inventory System" style="width:66vh;height:66vh"></img>
                </div>
            </div>
        </section>
    @endif
@endsection
@section('page-js')
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $.extend( $.fn.dataTable.defaults, {
        responsive: false, 
        autoWidth: true,
        autoLength:true,
        serverSide: true,
        info:true,
        bFilter:true,
        lengthChange: true,
    });

    var table = $("#table-list").DataTable({
        ajax: {
            type: "GET",
            url: "{{ route('home.urutanOrder') }}",
        },
        createdRow: function(row, data, dataIndex) {
            var status = data.status;

            if(status === 'Selesai'){
                $(row).css('background-color', '#74ff76'); // Set background color for 'onprocess' status
            }else if (status === 'Dalam Proses Produksi') {
                $(row).css('background-color', '#fffa99'); // Set background color for 'onprocess' status
            } else {
                $(row).css('background-color', '#FF0060'); // Set background color for other statuses
            }
        },
        columns: [
            {data: 'id', name: 'id',searchable:true,visible:true,orderable:true},
            {data: 'invoice_number', name: 'invoice_number',searchable:true,visible:true,orderable:true},
            {data: 'client_order', name: 'client_order',searchable:true,visible:true,orderable:true},
            {data: 'status', name: 'status',searchable:true,visible:true,orderable:false},
            {data: 'created_at', name: 'created_at',searchable:true,visible:true,orderable:true},
        ]
    });
});
</script>
@endsection