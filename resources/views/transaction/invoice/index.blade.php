@extends('layouts.app',['active' => 'transaction_invoice'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Table Invoice</h3>
                    </div>
                    <div class="card-body">
                        <table id="table-list" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Invoice No</th>
                                    <th>Nama CLient</th>
                                    <th>Metode Bayar</th>
                                    <th>Total Bayar</th>
                                    <th>Status</th>
                                    <th>Tanggal Order</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-js')
<script>
    function deletes(id){
        Swal.fire({
        title: 'Are you sure?',
        text: "To Delete This Invoice:"+id,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "get",
                    url: "/transaction/delete-invoice/"+id,
                    beforeSend: function () {
                            $('#table-list').block({ css: { 
                                border: 'none', 
                                padding: '10px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff',
                                fontSize:'20px',
                            },message: 'Please Wait...'
                        }); 
                    },
                    success: function (response) {
                        Swal.fire({
                            title:'Success!', 
                            text:'Deleted!',
                            icon:'success'
                        });
                        $('#table-list').DataTable.draw();
                    },
                    error: function(response){
                        $('#table-list').unblock();
                        if(response.status === 331){
                            var invoice = response.responseJSON;
                            Swal.fire({
                            title: 'This Invoice Already Production!',
                            text: "Are You Sure to Delete Invoice:"+invoice,
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Cancel Order!'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });
                                    $.ajax({
                                        type: "GET",
                                        url : "{{ route('invoice.cancelOrder') }}",
                                        data:{
                                            invoice:invoice
                                        },
                                        beforeSend: function () {
                                            $.blockUI({ css: { 
                                                border: 'none', 
                                                padding: '10px', 
                                                backgroundColor: '#000', 
                                                '-webkit-border-radius': '10px', 
                                                '-moz-border-radius': '10px', 
                                                opacity: .5, 
                                                color: '#fff',
                                                fontSize:'16px'
                                            },message: 'Please Wait...'
                                            }); 
                                        },
                                        success: function(response) {
                                            $.unblockUI();
                                            Swal.fire({
                                                title:'Success!', 
                                                text:'Canceled Order!',
                                                icon:'success'
                                            });
                                            table.draw();
                                        },
                                        error: function(response){
                                            $.unblockUI();
                                            if(response.status == 422){
                                                Swal.fire({
                                                    title:'Oopps..!', 
                                                    text:response.responseJSON, 
                                                    icon:'error'
                                                });
                                            }else{
                                                Swal.fire({
                                                    title:'Oopps..!', 
                                                    text:'Something Went Wrong!',
                                                    icon:'error'
                                                });
                                            }
                                        }
                                    });
                                }else{
                                    Swal.fire({
                                        title:'Canceled!', 
                                        text:'Nothing Change', 
                                        icon:'success',
                                        timer:900
                                    });
                                }
                            })
                        }else{
                            $('#table-list').unblock();
                            Swal.fire({
                                title:'Success!', 
                                text:response.toString(),
                                icon:'success'
                            });
                            $("#table-list").DataTable().ajax.reload();
                        }
                    }
                })
            }else{
                Swal.fire({
                    title:'Canceled!', 
                    text:'Nothing Change', 
                    icon:'success',
                    timer:900
                });
            }
        });
    }
    function prints(id){
        $.ajax({
            type: "get",
            url: "/transaction/print-invoice/"+id,
            beforeSend: function () {
                $('#table-list').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'20px',
                },message: 'Please Wait...'
            }); 
            },
            success: function (response) {
                
            },
            error: function(response){
                $('#table-list').unblock();
                if(response.status == 422){
                    Swal.fire({
                        title:'Oopps...!', 
                        text:response.responseJSON.toString(),
                        icon:'error'
                    });
                }else{
                    Swal.fire({
                        title:'Oopps...!', 
                        text:'Something Went Wrong!',
                        icon:'error',
                        timer:1000
                    });
                }
            }
        })
    }
</script>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $("#table-list").on("click", ".prints", function() {
        var id = $(this).data('invoice_no');
        prints(id);
    });
    $("#table-list").on("click", ".deletes", function() {
        var id = $(this).data('invoice_no');
        deletes(id);
    });
    
    $.extend( $.fn.dataTable.defaults, {
        responsive: false, 
        autoWidth: true,
        autoLength:true,
        serverSide: true,
        info:true,
        bFilter:true,
        lengthChange: true,
    });

    var table = $("#table-list").DataTable({
        ajax: {
            type: "GET",
            url: "{{ route('invoice.dataInvoice') }}",
        },
        createdRow: function(row, data, dataIndex) {
            var status = data.status;

            if(status === 'CANCEL'){
                $(row).css('background-color', '#FF1E1E'); // Set background color for other statuses
                $(row).css('color', '#FBFFDC'); // Set background color for other statuses
                $(row).css('font-weight', 'bold'); // Set background color for other statuses
            }
            
        },
        columns: [
            {data: 'invoice_number', name: 'invoice_number',searchable:true,visible:true,orderable:true},
            {data: 'client_order', name: 'client_order',searchable:true,visible:true,orderable:true},
            {data: 'metode_bayar', name: 'metode_bayar',searchable:true,visible:true,orderable:true},
            {data: 'total_price', name: 'total_price',searchable:true,visible:true,orderable:false},
            {data: 'status', name: 'status',searchable:true,visible:true,orderable:false},
            {data: 'created_at', name: 'created_at',searchable:true,visible:true,orderable:true},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ]
    });
});
</script>
@endsection