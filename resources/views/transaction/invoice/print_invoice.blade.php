@extends('layouts.app',['active' => 'transaction_order'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="invoice p-3 mb-3">
            <div class="row">
            <div class="col-12">
                <h4>
                <i class="fas fa-globe"></i> MEGAH JAYA SUKSES PRINTING
                <small class="float-right">{{ \Carbon\Carbon::now()->format('d M Y H:i:s') }}</small>
                </h4>
            </div>
            </div>
            <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                <address>
                <strong>Megah Jaya Sukses Printing</strong><br>
                Jl.Kayu Manis<br>
                Semarang<br>
                Phone: (024) 123-45678<br>
                Email: admin@megahjayasukses.co.id
                </address>
            </div>
            <div class="col-sm-4 invoice-col">
                To
                <address>
                <strong>{{ strtoupper($data->client_order) }}</strong><br>
                {{ $data->alamat == null ? '-' : $data->alamat }}<br>
                Phone: {{ $data->no_telp == null ? '-' : $data->no_telp }}<br>
                </address>
            </div>
            <div class="col-sm-4 invoice-col">
                <b>Invoice {{ $data->invoice_number }}</b><br>
                <br>
                <b>Payment Due:</b> {{ \Carbon\Carbon::now()->format('d M Y H:i:s') }}<br>
            </div>
            </div>
            <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                <thead>
                <tr>
                    <th>Product</th>
                    <th>Size</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Type</th>
                    <th>Subtotal</th>
                </tr>
                </thead>
                <tbody>
                @foreach($detail as $key => $val)
                <tr>
                    <td>{{ strtoupper($val->product_name) }}</td>
                    <td>{{ strtoupper($val->size) }}</td>
                    <td>{{ $data->qty }}</td>
                    <td>{{ 'Rp. '.number_format($val->total_price) }}</td>
                    <td>{{ strtoupper($val->process_name)  }}</td>
                    <td>{{ 'Rp. '.number_format( $data->qty * $val->total_price ) }}</td>
                </tr>
                @endforeach
                </tbody>
                </table>
            </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
            <!-- accepted payments column -->
            <div class="col-6">
                <p class="lead">Metode Pembayaran:</p>
                <img src="{{asset('admin_asset/img/credit/visa.png')}}" alt="Visa">
                <img src="{{asset('admin_asset/img/credit/mastercard.png')}}" alt="Mastercard">
                <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                Harap Cek kelengkapan hasil dari produk yang anda bawa,klaim hanya dapat dilayani 1 x 24 Jam setelah meninggalkan outlet.
                </p>
            </div>
            <!-- /.col -->
            <div class="col-6">
                <p class="lead">Amount Due {{ \Carbon\Carbon::now()->format('d M Y') }}</p>

                <div class="table-responsive">
                <table class="table">
                    <tr>
                    <th style="width:50%">Subtotal:</th>
                    <td>Rp. {{ number_format($data->total_price) }}</td>
                    </tr>
                    <tr>
                    <th>Pajak</th>
                    <td>{{ $data->pajak }} %</td>
                    </tr>
                    <tr>
                    <th>Total:</th>
                    <td>Rp. {{ number_format($data->total_price) }}</td>
                    </tr>
                </table>
                </div>
            </div>
            </div>
            <div class="row no-print">
            <div class="col-12">
                <a type="button" id="print_btn" target="_blank" class="btn btn-success"><i class="fas fa-print"></i> Print</a>
            </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-js')
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $('#print_btn').click(function(){
        window.print();
    });
});
</script>
@endsection