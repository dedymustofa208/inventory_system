<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ url('css/admin_asset/adminlte.css') }}">
    <link rel="stylesheet" href="{{ url('css/admin_asset/all.min.css') }}">
</head>
<body>
    <h4 style="font-family: Arial, sans-serif !important;">
        <i class="fas fa-globe"></i> MEGAH JAYA SUKSES PRINTING
        <small class="float-right">{{ \Carbon\Carbon::now()->format('d M Y H:i:s') }}</small>
    </h4>
    <hr>
    <div class="row invoice-info">
        <div class="col-md-12 invoice-col">
            <table style="width:100%">
                <tr>
                    <td>From</td>
                    <td></td>
                    <td>To</td>
                </tr>
                <tr>
                    <td><strong>Megah Jaya Sukses Printing</strong></td>
                    <td></td>
                    <td><strong>{{ strtoupper($data->client_order) }}</strong></td>
                </tr>
                <tr>
                    <td>Jl.Kayu Manis</td>
                    <td></td>
                    <td>{{ $data->alamat == null ? '-' : $data->alamat }}</td>
                </tr>
                <tr>
                    <td>Semarang</td>
                    <td></td>
                    <td>Phone: {{ $data->no_telp == null ? '-' : $data->no_telp }}</td>
                </tr>
                <tr>
                    <td>Phone: (024) 123-45678</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Email: admin@megahjayasukses.co.id</td>
                    <td></td>
                    <td><b>Invoice {{ $data->invoice_number }}</b></td>
                </tr>
            </table>
        </div>
        <table style="width:100%">
            <tr>
                <td style="width:33.33%"></td>
                <td style="width:18.89%"></td>
                <td style="width:33.33%">
                    <b>Payment Due:</b> {{ \Carbon\Carbon::now()->format('d M Y H:i:s') }}
                </td>
            </tr>
        </table>        
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped">
            <thead>
            <tr>
                <th>Product</th>
                <th>Size</th>
                <th>Qty</th>
                <th>Price</th>
                <th>Type</th>
                <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            @foreach($detail as $key => $val)
            <tr>
                <td>{{ strtoupper($val->product_name) }}</td>
                <td>{{ strtoupper($val->size) }}</td>
                <td>{{ $data->qty }}</td>
                <td>{{ 'Rp. '.number_format($val->total_price) }}</td>
                <td>{{ strtoupper($val->process_name)  }}</td>
                <td>{{ 'Rp. '.number_format( $data->qty * $val->total_price ) }}</td>
            </tr>
            @endforeach
            </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <table style="width:100%">
            <tr>
                <td><strong>Metode Pembayaran:<strong></td>
                <td></td>
                <td style="text-align:right"><p class="lead">Amount Due {{ \Carbon\Carbon::now()->format('d M Y') }}</p></td>
                <td></td>
            </tr>
            <tr>
                <td style="width:40%">
                    <img src="{{asset('admin_asset/img/credit/visa.png')}}" alt="Visa">
                    <img src="{{asset('admin_asset/img/credit/mastercard.png')}}" alt="Mastercard">
                    <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                    Harap Cek kelengkapan hasil dari produk yang anda bawa,klaim hanya dapat dilayani 1 x 24 Jam setelah meninggalkan outlet.
                    </p>
                </td>
                <td></td>
                <table style="width:100%">
                    <tr>
                        <td style="text-align:right">Subtotal:</td>
                        <td style="text-align:right">Rp. {{ number_format($data->total_price) }}</td>
                    </tr>
                    <tr>
                        <td style="text-align:right">Pajak:</td>
                        <td style="text-align:right">{{ $data->pajak }} %</td>
                    </tr>
                    <tr>
                        <td style="text-align:right">Total:</td>
                        <td style="text-align:right">Rp. {{ number_format($data->total_price) }}</td>
                    </tr>
                </table>
                <td></td>
            </tr>
        </table>
    </div>
</body>
</html>