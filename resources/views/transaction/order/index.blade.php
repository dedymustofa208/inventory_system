@extends('layouts.app',['active' => 'transaction_order'])

@section('content-header')
<section class="content-header">

</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark collapsed-card">
                    <div class="card-header">
                        <h3 class="card-title">Tambah Pesanan Baru</h3>
                        <div class="row card-tools pt-2">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body" style="display:none">
                        <form id="form_add" action="{{ route('order.addOrder') }}" method="post">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-3 mb-3">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input type="text" class="form-control client_name text-uppercase" autocomplete="off" placeholder="Nama Client" id="client_name" name="client_name" required>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                        </div>
                                        <input type="text" class="form-control no_telp text-uppercase" onkeypress="return numberOnly(event)" autocomplete="off" placeholder="No Telp" id="no_telp" name="no_telp" required>
                                    </div>
                                </div>
                                <div class="col-md-1 mb-3" hidden>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-percentage"></i></span>
                                        </div>
                                        <input type="text" class="form-control pajak text-uppercase" autocomplete="off" placeholder="Pajak" id="pajak" name="pajak" readonly>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control total_price text-uppercase" autocomplete="off" placeholder="Rp. Total Price" id="total_price" name="total_price" readonly>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-barcode"></i></span>
                                        </div>
                                        <input type="text" class="form-control invoice_number text-uppercase" autocomplete="off" placeholder="INV0000" id="invoice_number" name="invoice_number" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2 mt-2">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-box"></i></span>
                                        </div>
                                        <select class="form-control select type_bayar" id="type_bayar" name="type_bayar" data-placeholder="Metode Bayar" data-allow-clear="true" required>
                                            <option value="" disabled selected></option>
                                            @foreach($type_bayar as $key => $val)
                                                <option value="{{ $key }}">{{ strtoupper( $val) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 mt-2">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-percentage"></i></span>
                                        </div>
                                        <input type="text" class="form-control diskon text-uppercase" onkeypress="return numberOnly(event)" autocomplete="off" placeholder="Diskon" id="diskon" name="diskon" required>
                                    </div>
                                </div>
                                <div class="col-md-2 mt-2">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">PCS</span>
                                        </div>
                                        <input type="text" class="form-control bonus text-uppercase" onkeypress="return numberOnly(event)" autocomplete="off" placeholder="bonus" id="bonus" name="bonus" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-file-alt"></i></span>
                                        </div>
                                        <textarea type="text" class="form-control catatan text-uppercase" placeholder="Catatan"  autocomplete="off" id="catatan" name="catatan"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-home"></i></span>
                                        </div>
                                        <textarea type="text" class="form-control alamat text-uppercase" placeholder="Alamat"  autocomplete="off" id="alamat" name="alamat"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12" id="repeater">
                                    <div class="form-group row entry">
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                                                </div>
                                                <select class="form-control selects barang" id="barang" name="barang[]" data-placeholder="Pilih Barang" data-allow-clear="true" required>
                                                    <option value="" disabled selected></option>
                                                    @foreach($product as $p)
                                                        <option value="{{ $p->id }}">{{ strtoupper( $p->barang->description.'::'.$p->process->description.'::'.$p->size ) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                                                </div>
                                                <input type="text" class="form-control qty text-uppercase" onkeypress="return numberOnly(event)" autocomplete="off" placeholder="Jumlah Pesanan" id="qty" name="qty[]" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-1 text-right pl-2">
                                            <button type="button" class="btn btn-success btn-add" id="plus-elem"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-success" id="button_add"><i class="fas fa-save"></i>&nbsp;Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Table Order</h3>
                    </div>
                    <div class="card-body">
                        <table id="table-list" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nama    Client</th>
                                    <th>No Telp</th>
                                    <th>Alamat </th>
                                    <th>Produk</th>
                                    <th>Jumlah</th>
                                    <th>Total Harga</th>
                                    <th>Metode</th>
                                    <th>No Invoice</th>
                                    <th>Tanggal Order</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-js')
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $('.select').select2({
        theme: 'bootstrap4'
    });
    $(function () {
        bsCustomFileInput.init();
    });

    let selectedItems = [];
    $("#repeater").on("click", ".btn-add", function () {
        let entry = $(this).closest(".entry").clone();
        let index = $("#repeater .entry").length;
        entry.find("select").each(function () {
            let oldId = $(this).attr("id") || "";
            let newId = oldId.includes("_") ? oldId.split("_")[0] + "_" + index : oldId + "_" + index;
            let newName = $(this).attr("name").replace(/\[\d*\]/, `[${index}]`);
            $(this).attr("id", newId).attr("name", newName);
            $(this).find("option").each(function () {
                if (selectedItems.includes($(this).val())) {
                    $(this).prop("disabled", true);
                }
            });
        });

        entry.find("input").each(function () {
            let oldId = $(this).attr("id") || "";
            let newId = oldId.includes("_") ? oldId.split("_")[0] + "_" + index : oldId + "_" + index;
            let newName = $(this).attr("name").replace(/\[\d*\]/, `[${index}]`);
            $(this).attr("id", newId).attr("name", newName).val("");
        });

        entry.find(".btn-add")
            .removeClass("btn-success btn-add")
            .addClass("btn-danger btn-remove")
            .html('<i class="fa fa-minus"></i>');

        $("#repeater").append(entry);
        entry.find('.selects').select2({ theme: 'bootstrap4' });
    });

    $("#repeater").on("click", ".btn-remove", function () {
        let entry = $(this).closest(".entry");
        let barang = entry.find("select[name^='barang']").val();
        selectedItems = selectedItems.filter(item => item !== barang);
        entry.remove();
        $("#repeater select").each(function () {
            $(this).find("option").each(function () {
                if (!selectedItems.includes($(this).val())) {
                    $(this).prop("disabled", false);
                }
            });
        });
    });

    $('#repeater').on('change', 'select', function () {
        let barang = $(this).val();
        if (selectedItems.includes(barang)) {
            Swal.fire({
                title: 'Oopps...!',
                text: 'Barang sudah dipilih!',
                icon: 'error',
                timer: 3000
            });
            $(this).val('').trigger('change');
            return false;
        }
        selectedItems.push(barang);
    });
    $("#repeater").on('blur', 'input', function(){
        let qty = $(this).val();
        let entry = $(this).closest(".entry");
        let barang = entry.find(".barang").val();
        let prevQty = entry.data('prevQty') || 0;

        if (qty === '' || qty <= 0) {
            $(this).addClass('is-invalid');
            Swal.fire({
                title: 'Oopps...!',
                text: 'Qty Harus Lebih Dari 0!',
                icon: 'error',
                timer: 3000
            });
            return false;
        } else {
            $(this).removeClass('is-invalid');
        }

        if ($('#client_name').val() !== '' && barang === '') {
            Swal.fire({
                title: 'Oopps...!',
                text: 'Barang Belum Dipilih!',
                icon: 'error',
                timer: 3000
            });
            $(this).val('');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "{{ route('order.totalPrice') }}",
            data: { barang: barang, qty: qty },
            beforeSend: function () {
                $.blockUI({ 
                    css: {
                        border: 'none',
                        padding: '10px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff',
                        fontSize: '16px',
                    },
                    message: 'Please Wait...'
                });
            },
            success: function(response) {
                $.unblockUI();
                let tmpTp = Number($('input[name="total_price"]').val().replace(/,/g, '') || 0);
                let total_price_new = Number(response || 0);
                let total_price = tmpTp - (prevQty * (total_price_new / qty)) + total_price_new;
                let formatted_totals = total_price.toLocaleString();
                if (parseInt(qty) >= 10000) {
                    entry.find('.diskon').val(0.5).prop('readonly', true);
                    entry.find('.bonus').val(5).prop('readonly', true);
                    entry.find('.pajak').val('11%');
                } else {
                    entry.find('.diskon').val(0);
                    entry.find('.bonus').val(0);
                }
                $('input[name="total_price"]').val(formatted_totals);
                entry.data('prevQty', qty);
            },
            error: function() {
                $.unblockUI();
                Swal.fire({
                    title: 'Oopps...!',
                    text: 'Something Went Wrong!',
                    icon: 'error',
                    timer: 1000
                });
            }
        });
    });

    $("#table-list").on("click", ".edit", function() {
        var id = $(this).data('id');
        var url = "/master/edit-product"+"/"+id;
        edit(url);
    });

    $("#table-list").on("click", ".deletes", function() {
        var id = $(this).data('id');
        deletes(id);
    });

    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        serverSide:true,
        deferRender: true,
        info:true,
        bFilter:true,
        lengthChange: true,
    });

    var table = $("#table-list").DataTable({
        ajax: {
            type: "GET",
            url: "{{ route('order.dataOrder') }}",
        },
        columns: [
            {data: 'client_order', name: 'client_order',searchable:true,visible:true,orderable:true},
            {data: 'no_telp', name: 'no_telp',searchable:true,visible:true,orderable:true},
            {data: 'alamat', name: 'alamat',searchable:true,visible:true,orderable:true},
            {data: 'product_id', name: 'product_id',searchable:true,visible:true,orderable:true},
            {data: 'qty', name: 'qty',searchable:false,visible:true,orderable:true},
            {data: 'total_price', name: 'total_price',searchable:false,visible:true,orderable:true},
            {data: 'metode_bayar', name: 'metode_bayar',searchable:true,visible:true,orderable:true},
            {data: 'invoice_number', name: 'invoice_number',searchable:true,visible:true,orderable:true},
            {data: 'created_at', name: 'created_at',searchable:true,visible:true,orderable:true},
            {data: 'is_completed', name: 'is_completed',searchable:true,visible:true,orderable:true}
        ]
    });

    table.draw();

    $('#button_add').on('click',function(){
        event.preventDefault();
        if($('#client_name').val() === ''){
            Swal.fire({
                title:'Oopps...!',
                text:'Nama Client Tidak Boleh Kosong!',
                icon:'error',
                timer:3000
            });
            return false;
        }
        if($('#client_name').val() !== ''){
            if($('#barang').val() === ''){
                Swal.fire({
                    title:'Oopps...!',
                    text:'Barang Belum Dipilih!',
                    icon:'error',
                    timer:3000
                });
                return false;
            }
            if($('#qty').val() === ''){
                Swal.fire({
                    title:'Oopps...!',
                    text:'Qty Tidak Boleh Kosong!',
                    icon:'error',
                    timer:3000
                });
                return false;
            }
            if($('#type_bayar').val() === null){
                Swal.fire({
                    title:'Oopps...!',
                    text:'Harap Pilih Tipe Pembayaran!',
                    icon:'error',
                    timer:3000
                });
                return false;
            }
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : $('#form_add').attr('action'),
            data: $('#form_add').serialize(),
            beforeSend: function () {
                $.blockUI({ css: {
                    border: 'none',
                    padding: '10px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff',
                    fontSize:'16px'
                },message: 'Please Wait...'
                });
            },
            success: function(response) {
                $.unblockUI();
                Swal.fire({
                    title:'Success!',
                    text:'Sukses Membuat Order!',
                    icon:'success'
                });
                setTimeout(() => {
                    location.reload();
                }, 100);
            },
            error: function(response){
                $.unblockUI();
                if(response.status === 422){
                    Swal.fire({
                        title:'Warning!',
                        text:response.responseJSON,
                        icon:'warning'
                    });
                }else{
                    Swal.fire({
                        title:'Oopps...!',
                        text:'Something Went Wrong!',
                        icon:'error',
                        timer:1000
                    });
                }
            }
        });
    });

    $('#client_name').blur(function(){
        const client_name = $(this).val();
        if (client_name == '') {
            $('.client_name').addClass('is-invalid');
        } else {
            $('.client_name').removeClass('is-invalid');
        }
    });

    $('#no_telp').blur(function(){
        const no_telp = $(this).val();
        if (no_telp == '') {
            $('.no_telp').addClass('is-invalid');
        } else {
            $('.no_telp').removeClass('is-invalid');
        }
    });

    $('#diskon').blur(function(){
        var diskon = $(this).val();
        if (diskon == '') {
            $('.qty').addClass('is-invalid');
            Swal.fire({
                title:'Oopps...!',
                text:'Masukkan Diskon Jika Ada!',
                icon:'info',
                timer:3000
            });
            return false;
        } else {
            $('.qty').removeClass('is-invalid');
        }
        var barang  = $('#barang').val();
        var qty     = $('#qty').val();
        if($('#client_name').val() !== ''){
            if(barang.length == 0) {
                Swal.fire({
                    title:'Oopps...!',
                    text:'Barang Belum Dipilih!',
                    icon:'error',
                    timer:3000
                });
                $('#qty').val('');
                return false;
            }
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : "{{ route('order.totalBasedDisc') }}",
            data: {
                barang : barang,
                qty : qty,
                diskon : diskon
            },
            beforeSend: function () {
                $.blockUI({ css: {
                    border: 'none',
                    padding: '10px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff',
                    fontSize:'16px',
                },message: 'Please Wait...'
                });
            },
            success: function(response) {
                $.unblockUI();
                $('#total_price').val(response.toLocaleString());
            },
            error: function(response){
                $.unblockUI();
                Swal.fire({
                    title:'Oopps...!',
                    text:'Something Went Wrong!',
                    icon:'error',
                    timer:1000
                });
            }
        });
    });

    $('#type_bayar').change(function(){
        const type_bayar = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : "{{ route('order.genInvoice') }}",
            data: {
                type_bayar : type_bayar
            },
            beforeSend: function () {
                $.blockUI({ css: {
                    border: 'none',
                    padding: '10px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff',
                    fontSize:'16px',
                },message: 'Please Wait...'
                });
            },
            success: function(response) {
                $.unblockUI();
                $('#invoice_number').val(response);
            },
            error: function(response){
                $.unblockUI();
                Swal.fire({
                    title:'Oopps...!',
                    text:'Something Went Wrong!',
                    icon:'error',
                    timer:1000
                });
            }
        });
    });
});
</script>
@endsection
