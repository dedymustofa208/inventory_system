@extends('layouts.app',['active' => 'transaction_production'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Production Orders</h3>
                    </div>
                    <div class="card-body">
                        <table id="table-list" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Urutan</th>
                                    <th>Nomor Invoice</th>
                                    <th>Nama CLient</th>
                                    <th>Status</th>
                                    <th>Tanggal Pesan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-modal')
    <div class="modal fade" id="modal_production">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <input type="hidden" class="form-control" id="order_info_" readonly/>
                    <h4 class="modal-title">Detail Orders: <span class="font-weight-bold" id="order_info"></span></h4>
                    <button type="button" class="close btn_close_mod" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <table id="tableO" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nama Barang</th>
                                    <th>Size</th>
                                    <th>Proses</th>
                                    <th>Qty</th>
                                    <th>Status</th>
                                    <th>Order Date</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" id="processed" class="btn btn-primary">Processed</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function (event) {
        $(function () {
            bsCustomFileInput.init();
        });

        $('#material_embro,#material_print').select2({
            dropdownParent: $('#modal_production'),
            theme: 'bootstrap4'
        });
        $.extend( $.fn.dataTable.defaults, {
            serverSide:true,
            deferRender: true,
            info:true,
            bFilter:true,
            lengthChange: true
        });

        var table = $("#table-list").DataTable({
            ajax: {
                type: "GET",
                url: "{{ route('production.getDataProgress') }}",
            },
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'invoice_number', name: 'invoice_number',searchable:true,visible:true,orderable:true},
                {data: 'client_order', name: 'client_order',searchable:true,visible:true,orderable:true},
                {data: 'status', name: 'status',searchable:true,visible:true,orderable:true},
                {data: 'created_at', name: 'created_at',searchable:true,visible:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,orderable:false},
            ]
        });

        $("#table-list").on("click", ".proses", function(e) {
            event.preventDefault();
            var inv = $(this).data('invoice_no');

            Swal.fire({
                title: 'Are you sure?',
                text: "Process This Invoice:"+inv,
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Process it!',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "GET",
                        url : "{{ route('production.confirmOrder') }}",
                        data:{
                            invoice:inv
                        },
                        beforeSend: function () {
                            $.blockUI({ css: { 
                                border: 'none', 
                                padding: '10px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff',
                                fontSize:'16px'
                            },message: 'Please Wait...'
                            }); 
                        },
                        success: function(response) {
                            var inv = response.invoice;
                            var client_order = response.client_order;
                            $.unblockUI();
                            $('#modal_production').modal('show');
                            $('#order_info_').val(inv);
                            $('#order_info').text(inv+'-'+client_order.toUpperCase());
                            var button = document.getElementById("processed");
                            button.setAttribute("data-invoicenum", inv);
                            tableOrder(inv);
                        },
                        error: function(response){
                            $.unblockUI();
                            Swal.fire({
                                title:'Oopps..!', 
                                text:response.responseJSON, 
                                icon:'error'
                            });
                        }
                    });
                }else{
                    Swal.fire({
                        title:'Canceled!', 
                        text:'Nothing Change', 
                        icon:'success',
                        timer:900
                    });
                }
            })
        });

        $("#table-list").on("click", ".selesai", function(e) {
            event.preventDefault();
            var inv = $(this).data('invoice_no');

            Swal.fire({
            title: 'Are you sure?',
            text: "Finished Production on Invoice:"+inv,
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Finish!',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "GET",
                        url : "{{ route('production.productionFinished') }}",
                        data:{
                            invoice:inv
                        },
                        beforeSend: function () {
                            $.blockUI({ css: { 
                                border: 'none', 
                                padding: '10px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff',
                                fontSize:'16px'
                            },message: 'Please Wait...'
                            }); 
                        },
                        success: function(response) {
                            $.unblockUI();
                            Swal.fire({
                                title:'Success!', 
                                text:'Production Finished!',
                                icon:'success'
                            });
                            table.draw();
                        },
                        error: function(response){
                            $.unblockUI();
                            if(response.status == 422){
                                Swal.fire({
                                    title:'Oopps..!', 
                                    text:response.responseJSON, 
                                    icon:'error'
                                });
                            }else{
                                Swal.fire({
                                    title:'Oopps..!', 
                                    text:'Something Went Wrong!',
                                    icon:'error'
                                });
                            }
                        }
                    });
                }else{
                    Swal.fire({
                        title:'Canceled!', 
                        text:'Nothing Change', 
                        icon:'success',
                        timer:900
                    });
                }
            })
        });

        $('#processed').click(function(event){
            event.preventDefault();
            var inv = $(this).data('invoicenum');
            Swal.fire({
                title: 'Are you sure?',
                text: "Processing for Production: "+inv+'!',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Process it!',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "GET",
                        url : "{{ route('production.processProduction') }}",
                        data:{
                            invoice:inv
                        },
                        beforeSend: function () {
                            $('#modal_production').block({ css: { 
                                border: 'none', 
                                padding: '10px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff',
                                fontSize:'16px'
                            },message: 'Please Wait...'
                            }); 
                        },
                        success: function(response) {
                            $('#modal_production').unblock();
                            $('#modal_production').modal('hide');
                            Swal.fire({
                                title:'Success', 
                                text:'Production On Going! on Invoice:'+inv,
                                icon:'success'
                            });
                            table.draw();
                        },
                        error: function(response){
                            $('#modal_production').unblock();
                            Swal.fire({
                                title:'Oopps..!', 
                                text: response.responseJSON,
                                icon:'error'
                            });
                        }
                    });
                }else{
                    Swal.fire({
                        title:'Canceled!', 
                        text:'Nothing Change', 
                        icon:'success',
                        timer:900
                    });
                }
            })
        });
    });

    function tableOrder(inv){
        tableO = $("#tableO").DataTable({
            serverSide:true,
            deferRender: true,
            info:true,
            bFilter:true,
            lengthChange: true,
            destroy:true,
            ajax: {
                type: "GET",
                data:{
                    invoice:inv
                },
                url: "{{ route('production.detailOrder') }}",
            },
            columns: [
                {data: 'nama_barang', name: 'nama_barang',searchable:true,visible:true,orderable:true},
                {data: 'size', name: 'size',searchable:true,visible:true,orderable:true},
                {data: 'process_name', name: 'process_name',searchable:true,visible:true,orderable:true},
                {data: 'qty', name: 'qty',searchable:true,visible:true,orderable:true},
                {data: 'status', name: 'status',searchable:true,visible:true,orderable:true},
                {data: 'order_date', name: 'order_date',searchable:true,visible:true,orderable:true},
            ]
        });
        tableO.clear();
        return tableO;
    }
</script>
@endsection