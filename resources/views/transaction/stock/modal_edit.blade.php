<div class="modal fade" id="modal_edit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Detail Stock</h4>
            <button type="button" class="close btn_close_mod" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="id_update" class="form-control" />
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                </div>
                <select class="form-control select warna" id="bahan_baku_update" name="bahan_baku_update" data-placeholder="Pilih Bahan Baku" data-allow-clear="true">
                    <option value="" disabled selected></option>
                    @foreach($bahan_baku as $key => $val)
                        <option value="{{ $val->id }}">{{ strtoupper($val->description) }}</option>
                    @endforeach
                </select>

                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                </div>
                <input type="text" class="form-control qty_stock_update text-uppercase" onkeypress="return numberOnly(event);" style="width:10%" autocomplete="off" placeholder="0" id="qty_stock_update" name="qty_stock_update" required></input>
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                </div>
                <input type="text" class="form-control price_stock_update text-uppercase" onkeypress="return numberOnly(event);" style="width:10%" autocomplete="off" placeholder="Rp. 0" id="price_stock_update" name="price_stock_update" required></input>

                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                </div>
                <select class="form-control select supplier_update" id="supplier_update" name="supplier_update" data-placeholder="Pilih Pemasok" data-allow-clear="true">
                    <option value="" disabled selected></option>
                    @foreach($supplier as $key => $val)
                        <option value="{{ $val->id }}">{{ strtoupper($val->description) }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default btn_close_mod" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="button_update"><i class="fas fa-save"></i>&nbsp;Save</button>
        </div>
        </div>
    </div>
</div>