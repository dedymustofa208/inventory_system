@extends('layouts.app',['active' => 'transaction_stock'])

@section('content-header')
<section class="content-header">

</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Table Stock Bahan Baku</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12 add_barang">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-box"></i></span>
                                </div>
                                <select class="form-control select warna" id="bahan_baku" name="bahan_baku" data-placeholder="Pilih Bahan Baku" data-allow-clear="true">
                                    <option value="" disabled selected></option>
                                    @foreach($bahan_baku as $key => $val)
                                        <option value="{{ $val->id }}">{{ strtoupper($val->code_bahan.'-'.$val->bahan_type) }}</option>
                                    @endforeach
                                </select>

                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-calculator"></i></span>
                                </div>
                                <input type="text" class="form-control qty_stock text-uppercase" title="Jumlah Stok" onkeypress="return numberOnly(event);" style="width:3%" autocomplete="off" placeholder="0" id="qty_stock" name="qty_stock" required></input>
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                </div>

                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-car-side"></i></span>
                                </div>
                                <select class="form-control select supplier" id="supplier" name="supplier" data-placeholder="Pilih Pemasok" data-allow-clear="true">
                                    <option value="" disabled selected></option>
                                    @foreach($supplier as $key => $val)
                                        <option value="{{ $val->id }}">{{ strtoupper($val->description) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>                      
                    </div>
                    <div class="card-footer">
                        <button type="button" class="btn btn-success pl-2" id="button_add"><i class="fas fa-save"></i>&nbsp;Save</button>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <table id="table-list" class="table table-bordered table-striped" style="width:100% !important">
                            <thead>
                                <tr>
                                    <th>Nama Bahan (Kode Bahan)</th>
                                    <th>Qty Stok</th>
                                    <th>Dipakai</th>
                                    <th>Sisa</th>
                                    <th>Satuan</th>
                                    <th>Pemasok</th>
                                    <th>Tanggal Beli</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-modal')
    @include('transaction.stock.modal_edit')
@endsection
@section('page-js')
<script>
    function edit(url){
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $('#table-list').block({ css: {
                    border: 'none',
                    padding: '10px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff',
                    fontSize:'20px',
                },message:'Please Wait...'});
            },
            success: function (response) {
                $('#table-list').unblock();
                var bahan_id = response.bahan_id;
                var qty_stock = response.qty;
                var uom = response.uom;
                var pemasok = response.pemasok;
                var id = response.id

                $('#id_update').val(id);
                $('#bahan_baku_update').val(bahan_id).change();
                $('#qty_stock_update').val(qty_stock);
                $('#supplier_update').val(pemasok).change();
                $('#uom_update').val(uom).change();
                $('#modal_edit').modal('show');
            }
        });
    }
</script>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $('.select').select2({
        theme: 'bootstrap4'
    });

    $("#table-list").on("click", ".edit", function() {
        var id = $(this).data('id');
        var url = "/transaction/edit-stock"+"/"+id;
        edit(url);
    });

    $(function () {
        bsCustomFileInput.init();
    });

    $("#table-list").on("click", ".deletes", function() {
        var id = $(this).data('id');
        deletes(id);
    });

    $.extend( $.fn.dataTable.defaults, {
        responsive: true,
        autoWidth: false,
        autoLength:false,
        serverSide: true,
        deferRender: true,
        info:true,
        bFilter:true,
        lengthChange: true,
    });

    var table = $("#table-list").DataTable({
        ajax: {
            type: "GET",
            url: "{{ route('stock.dataStock') }}",
        },
        columns: [
            {data: 'bahan_id', name: 'bahan_id',searchable:true,visible:true,orderable:true},
            {data: 'qty', name: 'qty',searchable:true,visible:true,orderable:true},
            {data: 'qty_used', name: 'qty_used',searchable:true,visible:true,orderable:true},
            {data: 'qty_available', name: 'qty_available',searchable:true,visible:true,orderable:true},
            {data: 'uom', name: 'uom',searchable:true,visible:true,orderable:true},
            {data: 'pemasok', name: 'pemasok',searchable:true,visible:true,orderable:true},
            {data: 'created_at', name: 'created_at',searchable:true,visible:true,orderable:true},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ]
    });

    $('#button_add').on('click',function(){
        event.preventDefault();
        var bahan_baku = $('#bahan_baku').val();
        var qty_stock = $('#qty_stock').val();
        var price_stock = $('#price_stock').val();
        var supplier = $('#supplier').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : "{{ route('stock.addStock') }}",
            data: {
                bahan_baku:bahan_baku,
                qty_stock:qty_stock,
                supplier:supplier,
            },
            beforeSend: function () {
                $.blockUI({ css: {
                    border: 'none',
                    padding: '10px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff',
                    fontSize:'16px'
                },message: 'Please Wait...'
                });
            },
            success: function(response) {
                $.unblockUI();
                $('#bahan_baku').val('').change();
                $('#qty_stock').val('');
                $('#supplier').val('').change();
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!',
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $.unblockUI();
                Swal.fire({
                    title:'Oopps...!',
                    text:response.responseJSON,
                    icon:'error'
                });
            }
        });
    });

    $('#qty_stock').blur(function(){
    var qty_stock = $(this).val();
        if (qty_stock == '') {
            $('.qty_stock').addClass('is-invalid');
        } else {
            $('.qty_stock').removeClass('is-invalid');
        }
    });

    $('#button_update').on('click',function(){
        var id_update = $('#id_update').val();
        var bahan_baku = $('#bahan_baku_update').val();
        var qty_stock = $('#qty_stock_update').val();
        var supplier = $('#supplier_update').val();
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : "{{ route('stock.updateStock') }}",
            data: {
                id_update:id_update,
                bahan_baku:bahan_baku,
                qty_stock:qty_stock,
                supplier:supplier,
            },
            beforeSend: function () {
                $('#modal_edit').block({ css: {
                    border: 'none',
                    padding: '10px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff',
                    fontSize:'16px',
                },message: 'Please Wait...'
                });
            },
            success: function(response) {
                $('#modal_edit').unblock();
                $('#modal_edit').modal('hide');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!',
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $('#modal_edit').unblock();
                Swal.fire({
                    title:'Oopps...!',
                    text:response.responseJSON,
                    icon:'error'
                });
                $('#modal_edit').modal('show');
            }
        });
    });
});
</script>
@endsection
