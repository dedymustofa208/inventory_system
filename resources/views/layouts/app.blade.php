<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>INVASYS</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/OverlayScrollbars.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_asset/select2.min.css') }}">
    <style type="text/css">
        ::-webkit-scrollbar {
        width: 5px;
        }

        ::-webkit-scrollbar-thumb {
        background-color: #888;
        }
    </style>
    @yield('page-css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    <div class="preloader flex-column justify-content-center align-items-center">
        <img class="" src="{{ asset('admin_asset/img/loader_ring2.gif') }}" alt="Inventory System" height="100" width="100">
    </div>
    <nav class="main-header navbar navbar-expand navbar-dark navbar-light">
        @include('admin.main_navbar')
    </nav>
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        @include('admin.side_navbar')
    </aside>
    <div class="content-wrapper">
        <script>
            function numberOnly(e){
                if (e.which != 8 && e.which != 46 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            }
        </script>
        @yield('content-header')
        @yield('page-content')
        @yield('page-modal')
        @yield('page-js')
        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function (event) {
                $('.btn_close_mod').click(function(){
                    $('#modal_edit').modal('hide');
                });
            })
        </script>
    </div>
    <footer class="main-footer">
        <strong>Copyright &copy; {{ Carbon\Carbon::now()->format('Y') }} <a href="#">INVENTORY | SYSTEM</a></strong>
        <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0
        </div>
    </footer>
</div>
<script src="{{ asset('js/admin_asset/jquery.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/bootstrap.bundle.min.js') }}"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/admin_asset/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/bootstrap.bundle.js') }}"></script>
<script src="{{ asset('js/admin_asset/bootstrap.js') }}"></script>
<script src="{{ asset('js/admin_asset/Chart.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/jquery.knob.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/moment.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/daterangepicker.js') }}"></script>
<script src="{{ asset('js/admin_asset/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/bs-custom-file-input.js') }}"></script>
<script src="{{ asset('js/admin_asset/dropzone.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/bs-custom-file-input.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/jszip.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/pdfmake.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/vfs_fonts.js') }}"></script>
<script src="{{ asset('js/admin_asset/buttons.print.min.js') }}"></script> 
<script src="{{ asset('js/admin_asset/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/toastr.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/adminlte.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/blockui.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/jquery.bootstrap-duallistbox.min.js') }}"></script>
<script src="{{ asset('js/admin_asset/select2.full.min.js') }}"></script>
</body>
</html>