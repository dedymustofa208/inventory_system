@extends('layouts.app',['active' => 'register'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header">
                        <h5>Tambah User Baru</h5>
                    </div>
                    <form id="form_add" action="{{ route('register.store') }}" enctype="multipart/form-data" method="post">
                        <div class="card-body">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                        </div>
                                        <input type="email" class="form-control email" autocomplete="off" placeholder="Email" id="email_user" name="email_user" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-transgender"></i></span>
                                        </div>
                                        <select class="form-control select gender" id="gender" name="gender">
                                            <option value="" disabled selected>--Pilih Jenis Kelamin--</option>
                                            <option value="male">Laki-laki</option>
                                            <option value="female">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input type="text" class="form-control username" placeholder="Nama" id="username" name="username" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                        </div>
                                        <input type="password" class="form-control password" placeholder="Password" id="password" name="password" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="photo_user" name="photo_user" accept="image/*">
                                            <label class="custom-file-label">Photo Profile</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-user-cog"></i></span>
                                        </div>
                                        <select class="form-control select role" id="role_user" name="role_user">
                                            @foreach($role as $r)
                                            <option value="{{ $r->id }}">{{ $r->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-dark">
                            <button type="button" class="btn btn-success" id="button_add"><i class="fas fa-save"></i>&nbsp;Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-dark">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Table User</h3>
                        </div>
                        <div class="card-body">
                        <table id="table-list" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nama User</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-js')
<script>
    function edit(url){
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $('#table-list').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'20px',
                },message:'Please Wait...'});
            },
            success: function (response) {
                $('#table-list').unblock();
            }
        })
        .then(function (response) {
            var name = response.name;
            var id = response.id

            $('#id_update').val(id);
            $('#role_name_update').val(name);
            $('#modal_edit').modal('show');
        });
    }
    // function deletes(id){
    //     $.ajax({
    //         type: "get",
    //         url: "role/delete-role/"+id,
    //         beforeSend: function () {
    //             $('#table-list').block({ css: { 
    //                 border: 'none', 
    //                 padding: '10px', 
    //                 backgroundColor: '#000', 
    //                 '-webkit-border-radius': '10px', 
    //                 '-moz-border-radius': '10px', 
    //                 opacity: .5, 
    //                 color: '#fff',
    //                 fontSize:'20px',
    //             },message: 'Please Wait...'
    //         }); 
    //         },
    //         success: function (response) {
    //             $('#table-list').unblock();
    //             $("#table-list").DataTable().ajax.reload();
    //             Swal.fire({
    //                 title:'Success!', 
    //                 text:response.toString(),
    //                 icon:'success'
    //             });
    //         },
    //         error: function(response){
                
    //             $('#table-list').unblock();
    //             if(response.status == 422){
    //                 Swal.fire({
    //                     title:'Oopps...!', 
    //                     text:response.responseJSON.toString(),
    //                     icon:'error'
    //                 });
    //             }else{
    //                 Swal.fire({
    //                     title:'Oopps...!', 
    //                     text:'Something Went Wrong!',
    //                     icon:'error',
    //                     timer:1000
    //                 });
    //             }
    //         }
    //     })
    // }
</script>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $('.select').select2({
        theme: 'bootstrap4'
    });
    $('#gender').on('change',function(){
        
    });
    $(function () {
        bsCustomFileInput.init();
    });

    $("#table-list").on("click", ".edit", function() {
        var id = $(this).data('id');
        var url = "/role/edit-data"+"/"+id;
        edit(url);
    });
    $("#table-list").on("click", ".deletes", function() {
        var id = $(this).data('id');
        deletes(id);
    });

    // $('#form_add').submit(function (event) {
    
    // $.extend( $.fn.dataTable.defaults, {
    //     responsive: false, 
    //     lengthChange: false, 
    //     autoWidth: true,
    //     autoLength:true,
    //     info:true,
    //     bFilter:true,
    //     lengthChange: false,
    //     scrollX:false,
    //     scrollY:false,
    // });

    var table = $("#table-list").DataTable({
        ajax: {
            type: "GET",
            url: "{{ route('register.data') }}",
        },
        columns: [
            {data: 'name', name: 'name',searchable:true,visible:true,orderable:false},
            {data: 'email', name: 'email',searchable:true,visible:true,orderable:false},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ]
    });

    $('#button_add').on('click',function(){
        event.preventDefault();
        var name        = $('#username').val();
        var email       = $('#email_user').val();
        var gender      = $('#gender').val();
        var password    = $('#password').val();
        var role        = $('#role_user').val();
        var reg = new RegExp(/[,()!~%`#$^&*=+;:'"<>|?]/g);
        if(reg.test(name)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#name').val('');
            return false;
        }
        if(reg.test(email)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#email').val('');
            return false;
        }
        var formData = new FormData(document.getElementById("form_add"));
        var parameter = ['email_user','gender','username','password','photo_user','role_user'];
        var val = [
            $('#email_user').val(),
            $('#gender').val(),
            $('#username').val(),
            $('#password').val(),
            $('#photo_user').val(),
            $('#role_user').val(),
        ];
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : $('#form_add').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#form_add').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px'
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $('#form_add').unblock();
                $('#form_add').trigger("reset");
                $('#role_user').val(null).trigger('change');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $('#form_add').unblock();
                Swal.fire({
                    title:'Oopps...!', 
                    text:response.responseJSON.toString(),
                    icon:'error',
                    timer:1000
                });
            }
        });
    });

    $('input[name="email_user"]').blur(function(){
    var emailValidator = /[@]/g;
    var email = $(this).val();
        if (!emailValidator.test(email)) {
            $('.email').addClass('is-invalid');
        } else {
            $('.email').removeClass('is-invalid');
        }
    });

    $('#username').blur(function(){
    var username = $(this).val();
        if (username == '') {
            $('.username').addClass('is-invalid');
        } else {
            $('.username').removeClass('is-invalid');
        }
    });

    $('#password').blur(function(){
    var password = $(this).val();
        if (password == '') {
            $('.password').addClass('is-invalid');
        } else {
            $('.password').removeClass('is-invalid');
        }
    });

    $('#button_update').on('click',function(){
        
        var name        = $('#name_update').val();
        var email       = $('#email_update').val();
        var gender      = $('#gender_update').val();
        var password    = $('#password_update').val();
        
        var role        = $('#role_update').val();
        var id_update = $('#id_update').val();
        var reg = new RegExp(/[,()!~%`#$^&*=+;:'"<>|?]/g);
        
        if(reg.test(name)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#name_update').val('');
            return false;
        }
        if(reg.test(email)){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#email_update').val('');
            return false;
        }
        
        event.preventDefault();
        var formData = new FormData(document.getElementById("form_update"));
        var parameter = ['id_update','permission_update'];
        var val = [
            $('#id_update').val(),
            $('#permission_update').val()
        ];
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : $('#form_update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#modal_edit').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px',
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $('#modal_edit').unblock();
                $('#modal_edit').modal('hide');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $('#modal_edit').unblock();
                Swal.fire({
                    title:'Oopps...!', 
                    text:'Something Went Wrong!',
                    icon:'error',
                    timer:1000
                });
            }
        });
    });
});
</script>
@endsection