<div class="modal fade" id="modal_edit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Permission</h4>
            <button type="button" class="close btn_close_mod" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form id="form_update" action="{{ route('permission.updateData') }}" enctype="multipart/form-data" method="post">
                <input type="hidden" id="id_update" class="form-control" />
                @csrf
                <input type="text" class="form-control w-100" id="permission_name_update" name="permission_name_update" placeholder="Permission Name">
            </form>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default btn_close_mod" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="button_update"><i class="fas fa-save"></i>&nbsp;Save</button>
        </div>
        </div>
    </div>
</div>