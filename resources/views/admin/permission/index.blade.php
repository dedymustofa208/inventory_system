@extends('layouts.app',['active' => 'permissions'])

@section('content-header')
<section class="content-header">
    
</section>
@endsection

@section('page-content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-dark">
                    <div class="card-header">
                        <h3 class="card-title">Tambah Permission</h3>
                    </div>
                    <form id="form_add" action="{{ route('permission.saveData') }}" enctype="multipart/form-data" method="post">
                        <div class="card-body">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label>Name</label>
                                    <input type="text" class="form-control" id="permission_name" name="permission_name" placeholder="Permission Name" required>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-primary" id="button_add"><i class="fas fa-save"></i>&nbsp;Save</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header bg-dark">
                    <h3 class="card-title">Table Permissions</h3>
                    </div>
                    <div class="card-body">
                    <table id="table-list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nama Permission</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-modal')
    @include('admin.permission.modal_edit')
@endsection
@section('page-js')
<script>
    function edit(url){
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $('#table-list').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'20px',
                },message:'Please Wait...'});
            },
            success: function (response) {
                $('#table-list').unblock();
            }
        })
        .then(function (response) {
            var name = response.name;
            var id = response.id

            $('#id_update').val(id);
            $('#permission_name_update').val(name);
            $('#modal_edit').modal('show');
        });
    }
    function deletes(id){
        $.ajax({
            type: "get",
            url: "permission/delete-permission/"+id,
            beforeSend: function () {
                $('#table-list').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'20px',
                },message: 'Please Wait...'
            }); 
            },
            success: function (response) {
                $('#table-list').unblock();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(), 
                    icon:'success'
                });
                $("#table-list").DataTable().ajax.reload();
            },
            error: function(response){
                
                $('#table-list').unblock();
                if(response.status == 422){
                    Swal.fire({
                        title:'Oopps...!', 
                        text:response.responseJSON.toString(),
                        icon:'error'
                    });
                }else{
                    Swal.fire({
                        title:'Oopps...!', 
                        text:'Something Went Wrong!',
                        icon:'error'
                    });
                }
            }
        })
    }
</script>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function (event) {
    $(function () {
        bsCustomFileInput.init();
    });

    $("#table-list").on("click", ".edit", function() {
        var id = $(this).data('id');
        var url = "/permission/edit-data"+"/"+id;
        edit(url);
    });
    $("#table-list").on("click", ".deletes", function() {
        var id = $(this).data('id');
        deletes(id);
    });

    // $('#form_add').submit(function (event) {
    
    $.extend( $.fn.dataTable.defaults, {
        responsive: true, 
        lengthChange: false, 
        autoWidth: true,
        info:false,
        bFilter:false,
        lengthChange: false,
    });

    var table = $("#table-list").DataTable({
        ajax: {
            type: "GET",
            url: "{{ route('permission.getData') }}",
        },
        columns: [
            {data: 'name', name: 'name',searchable:true,visible:true,orderable:false},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ]
    });

    $('#button_add').on('click',function(){
        event.preventDefault();
        var permission_name = $('#permission_name').val();
        var reg = new RegExp(/[,()!~%`@#$^&*=+;:'"<>|?]/g);
        if(reg.test($('#permission_name').val())){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error'
            });
            $('#permission_name').val('');
            return false;
        }
        var formData = new FormData(document.getElementById("form_add"));
        var parameter = ['permission_name'];
        var val = [
            $('#permission_name').val()
        ];
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : $('#form_add').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#form_add').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px'
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $('#form_add').unblock();
                $('#form_add').trigger("reset");
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success',
                    timer:900
                });
                $('#permission_name').val('').focus();
            },
            error: function(response){
                $('#form_add').unblock();
                Swal.fire({
                    title:'Oopps...!', 
                    text:response.responseJSON,
                    icon:'error',
                    timer:900
                });
                $('#permission_name').val('').focus();
            }
        });
    });

    $('#button_update').on('click',function(){
        var permission_update = $('#permission_name_update').val();
        var id_update = $('#id_update').val();
        var reg = new RegExp(/[,()!~%`@#$^&*=+;:'"<>|?]/g);
        if(reg.test($('#permission_name_update').val())){
            Swal.fire({
                title:'Oopps...!', 
                text:'SPESIAL KARAKTER TIDAK DI IZINKAN!',
                icon:'error',
                timer:1000
            });
            $('#permission_name_update').val('');
            return false;
        }
        event.preventDefault();
        var formData = new FormData(document.getElementById("form_update"));
        var parameter = ['id_update','permission_name_update'];
        var val = [
            $('#id_update').val(),
            $('#permission_name_update').val()
        ];
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url : $('#form_update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#modal_edit').block({ css: { 
                    border: 'none', 
                    padding: '10px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff',
                    fontSize:'16px',
                },message: 'Please Wait...'
                }); 
            },
            success: function(response) {
                $('#modal_edit').unblock();
                $('#modal_edit').modal('hide');
                $("#table-list").DataTable().ajax.reload();
                Swal.fire({
                    title:'Success!', 
                    text:response.toString(),
                    icon:'success'
                });
            },
            error: function(response){
                $('#modal_edit').unblock();
                Swal.fire({
                    title:'Oopps...!', 
                    text:'Something Went Wrong!',
                    icon:'error',
                    timer:1000
                });
            }
        });
    });
});
</script>
@endsection