<div class="modal fade" id="modal_edit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Role</h4>
            <button type="button" class="close btn_close_mod" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
                <div class="card card-dark">
                    <div class="card-header">
                        <h3 class="card-title">Edit Role</h3>
                    </div>
                    <form id="form_update" action="{{ route('role.updateData') }}" enctype="multipart/form-data" method="post">
                        <input type="hidden" class="form-controll" name="id_update" id="id_update" />
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Role Name</label>
                                    <input type="text" class="form-control" id="role_name_update" name="role_name_update" placeholder="Masukkan Nama Kursus" required>
                                </div>
                                <div class="col-md-12 select2-success">
                                    <label>Permission Name</label>
                                    <select class="select2" id="permission_name_update" name="permission_name_update[]" multiple="multiple" data-placeholder="Select a State" style="width: 100%;">
                                        @foreach($permission as $p)
                                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
        <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default btn_close_mod" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="button_update"><i class="fas fa-save"></i>&nbsp;Update</button>
        </div>
        </div>
    </div>
</div>