<a href="index3.html" class="brand-link">
    <b><h5 class="brand-text font-weight-light text-center"><b>INVENTORY SYSTEM</b></h5></b>
</a>

<div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ asset('admin_asset/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block">{{ strtoupper(\Auth::user()->name) }}</a>
        </div>
    </div>
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            @canany(['menu-permissions','menu-roles','menu-users'])
                <li class="nav-header">SUPER USER</li>
                <li class="nav-item {{in_array($active,['permissions','roles','register']) ? 'menu-open' : ''}}">
                    <a href="#" class="nav-link {{in_array($active,['permissions','roles','register']) ? 'active' : ''}}">
                        <i class="nav-icon fas fa-user-cog"></i><p>Administrator<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('menu-permissions')
                        <li class="nav-item">
                            <a href="{{ route('permission.index') }}" class="nav-link {{ $active == 'permissions' ? 'active' : '' }}">
                                <i class="fas fa-cog nav-icon text-info"></i><p>Permissions</p>
                            </a>
                        </li>
                        @endcan
                        @can('menu-roles')
                        <li class="nav-item">
                            <a href="{{ route('role.index') }}" class="nav-link {{ $active == 'roles' ? 'active' : '' }}">
                                <i class="fas fa-lock nav-icon text-info"></i><p>Roles</p>
                            </a>
                        </li>
                        @endcan
                        @can('menu-users')
                        <li class="nav-item">
                            <a href="{{ route('register.index') }}" class="nav-link {{ $active == 'register' ? 'active' : '' }}">
                                <i class="far fa-user nav-icon text-info"></i><p>Users</p>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>
            @endcanany
            <li class="nav-header"></li>
            @can('menu-dashboard')
            <li class="nav-item">
                <a href="{{ route('home') }}" class="nav-link {{ $active == 'dashboard' ? 'active' : '' }}">
                    <i class="fas fa-tachometer-alt nav-icon text-white"></i><p>Dashboard</p>
                </a>
            </li>
            @endcan
            @canany(['master-barang','master-process','master-product','master-size','master-supplier','master-bahan-baku'])
                <li class="nav-header">MASTER</li>
                @can('master-supplier')
                <li class="nav-item">
                    <a href="{{ route('supplier.index') }}" class="nav-link {{ $active == 'master_supplier' ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-white"></i>
                        <p>Pemasok</p>
                    </a>
                </li>
                @endcan
                @can('master-bahan-baku')
                <li class="nav-item">
                    <a href="{{ route('bahan_baku.index') }}" class="nav-link {{ $active == 'master_bahan_baku' ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-pink"></i>
                        <p>Bahan Baku</p>
                    </a>
                </li>
                @endcan
                @can('master-barang')
                <li class="nav-item">
                    <a href="{{ route('barang.index') }}" class="nav-link {{ $active == 'master_barang' ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-success"></i>
                        <p>Jenis Produk</p>
                    </a>
                </li>
                @endcan
                @can('master-process')
                <li class="nav-item">
                    <a href="{{ route('process.index') }}" class="nav-link {{ $active == 'process' ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-warning"></i>
                        <p>Proses</p>
                    </a>
                </li>
                @endcan
                @can('master-size')
                <li class="nav-item">
                    <a href="{{ route('size.index') }}" class="nav-link {{ $active == 'master_size' ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-indigo"></i>
                        <p>Ukuran</p>
                    </a>
                </li>
                @endcan
                @can('master-product')
                <li class="nav-item">
                    <a href="{{ route('product.index') }}" class="nav-link {{ $active == 'master_product' ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-danger"></i>
                        <p>Produk</p>
                    </a>
                </li>
                @endcan
            @endcanany
            @canany(['transaction-invoice','transaction-stock','transaction-order','transaction-production'])
                <li class="nav-header">TRANSAKSI</li>
                @can('transaction-stock')
                <li class="nav-item">
                    <a href="{{ route('stock.index') }}" class="nav-link {{ $active == 'transaction_stock' ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-indigo"></i>
                        <p>Stok Bahan Baku</p>
                    </a>
                </li>
                @endcan
                @can('transaction-order')
                <li class="nav-item">
                    <a href="{{ route('order.index') }}" class="nav-link {{ $active == 'transaction_order' ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-success"></i>
                        <p>Pesanan</p>
                    </a>
                </li>
                @endcan
                @can('transaction-invoice')
                <li class="nav-item">
                    <a href="{{ route('invoice.index') }}" class="nav-link {{ $active == 'transaction_invoice' ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-warning"></i>
                        <p>Tagihan</p>
                    </a>
                </li>
                @endcan
                @can('transaction-production')
                <li class="nav-item">
                    <a href="{{ route('production.index') }}" class="nav-link {{ $active == 'transaction_production' ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-pink"></i>
                        <p>Proses Produksi</p>
                    </a>
                </li>
                @endcan
            @endcanany
            @canany(['laporan-transaki','laporan-invoice','laporan-stock'])
            <li class="nav-header">LAPORAN</li>
            @can('laporan-transaksi')
            <li class="nav-item">
                <a href="{{ route('report.transaksi') }}" class="nav-link {{ $active == 'laporan_transaksi' ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon text-success"></i>
                    <p>Transaksi</p><span class="right badge badge-danger"></span>
                </a>
            </li>
            @endcan
            @can('laporan-invoice')
            <li class="nav-item">
                <a href="{{ route('report.tagihan') }}" class="nav-link {{ $active == 'laporan_tagihan' ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon text-pink"></i>
                    <p>Tagihan</p>
                </a>
            </li>
            @endcan
            @can('laporan-stock')
            <li class="nav-item">
                <a href="{{ route('report.stock') }}" class="nav-link {{ $active == 'laporan_stock' ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon text-white"></i>
                    <p>Stok Bahan Baku</p>
                </a>
            </li>
            @endcan
            @endcanany
            <hr>
            <div class="col-md-12 text-center pb-3">
                <a href="{{ route('logout') }}" class="btn btn-danger w-100 text-white font-weight-bold" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>&nbsp;&nbsp;LOGOUT</a>
            </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>