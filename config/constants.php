<?php
return [
    'type_bayar' => [
        'cash' => 'CASH',
        'debit' => 'DEBIT',
        'kartu_kredit' => 'KARTU KREDIT'
    ],
    'bahan_type' => [
        'cat'       => 'CAT',
        'benang'    => 'BENANG',
        'mica'      => 'MICA',
        'alcohol'   => 'alcohol'
    ],
    'uom'   => [
        'liter' => 'Liter',
        // 'ml'    => 'Mililiter',
        'pcs'   => 'Pcs',
        // 'm' => 'Meter',
        'cones' => 'Cones',
        // 'cm'    => 'Centimeter',
        // 'kg'    => 'KG'
    ],
    'uom_bom' => [
        'm' => 'Meter',
        'ml' => 'Mililiter',
        'pcs' => 'Pcs'
    ],
    'warna' => [
        'non_color' => 'NON COLOR',
        'hitam' => 'HITAM',
        'putih' => 'PUTIH',
        'merah' => 'MERAH',
        'biru' => 'BIRU',
        'kuning' => 'KUNING',
        'hijau' => 'HIJAU',
        'ungu' => 'UNGU',
        'oranye' => 'ORANJE',
        'cokelat' => 'COKELAT',
        'abu_abu' => 'ABU-ABU',
        'pink' => 'PINK',
        'biru_tua' => 'BIRU TUA',
        'jingga' => 'JINGGA',
        'emas' => 'EMAS',
        'perak' => 'PERAK',
        'zaitun' => 'ZAITUN',
        'turquoise' => 'TURQUOISE',
        'magenta' => 'MAGENTA',
        'koral' => 'KORAL',
        'teal' => 'TEAL',
        'violet' => 'VIOLET'
    ],
    'conversion' => [
        'cat' => 1000,
        'alcohol' => 1000,
        'benang' => 5000,
        'mica' => 1
    ],
    'limit_stock' => [
        'cat' => 500,
        'alcohol' => 500,
        'benang' => 5000,
        'mica' => 6
    ],
    'uom_based' => [
        'benang' => [
            'm','cones','cm'
        ],
        'mica' => [
            'pcs'
        ],
        'alcohol' => [
            'liter', 'ml'
        ],
        'cat' => [
            'ml','liter'
        ]
    ]
];