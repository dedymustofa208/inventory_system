const mix = require('laravel-mix');
const webpack = require('webpack');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
.scripts('resources/admin_assets/jquery/jquery.min.js','public/js/admin_asset/jquery.min.js')
.scripts('resources/admin_assets/bootstrap/js/bootstrap.bundle.min.js','public/js/admin_asset/bootstrap.bundle.min.js')
.scripts('resources/admin_assets/bootstrap/js/bootstrap.min.js','public/js/admin_asset/bootstrap.min.js')
.scripts('resources/admin_assets/bootstrap/js/bootstrap.bundle.js','public/js/admin_asset/bootstrap.bundle.js')
.scripts('resources/admin_assets/bootstrap/js/bootstrap.js','public/js/admin_asset/bootstrap.js')
.scripts('resources/admin_assets/dist/js/adminlte.min.js','public/js/admin_asset/adminlte.min.js')
.scripts('resources/admin_assets/js/jquery-ui/jquery-ui.min.js','public/js/admin_asset/jquery-ui.min.js')
.scripts('resources/admin_assets/js/chart.js/Chart.min.js','public/js/admin_asset/Chart.min.js')
.scripts('resources/admin_assets/js/jquery-knob/jquery.knob.min.js','public/js/admin_asset/jquery.knob.min.js')
.scripts('resources/admin_assets/js/moment/moment.min.js','public/js/admin_asset/moment.min.js')
.scripts('resources/admin_assets/js/daterangepicker/daterangepicker.js','public/js/admin_asset/daterangepicker.js')
.scripts('resources/admin_assets/js/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js','public/js/admin_asset/tempusdominus-bootstrap-4.min.js')
.scripts('resources/admin_assets/js/overlayScrollbars/js/jquery.overlayScrollbars.min.js','public/js/admin_asset/jquery.overlayScrollbars.min.js')
.scripts('resources/admin_assets/js/bs-custom-file-input/bs-custom-file-input.min.js','public/js/admin_asset/bs-custom-file-input.min.js')
.scripts('resources/admin_assets/js/bs-custom-file-input/bs-custom-file-input.js','public/js/admin_asset/bs-custom-file-input.js')
.scripts('resources/admin_assets/js/dropzone/min/dropzone.min.js','public/js/admin_asset/dropzone.min.js')
.scripts('resources/admin_assets/js/datatables/jquery.dataTables.min.js','public/js/admin_asset/jquery.dataTables.min.js')
.scripts('resources/admin_assets/js/datatables-bs4/js/dataTables.bootstrap4.min.js','public/js/admin_asset/dataTables.bootstrap4.min.js')
.scripts('resources/admin_assets/js/datatables-responsive/js/dataTables.responsive.min.js','public/js/admin_asset/dataTables.responsive.min.js')
.scripts('resources/admin_assets/js/datatables-responsive/js/responsive.bootstrap4.min.js','public/js/admin_asset/responsive.bootstrap4.min.js')
.scripts('resources/admin_assets/js/datatables-buttons/js/dataTables.buttons.min.js','public/js/admin_asset/dataTables.buttons.min.js')
.scripts('resources/admin_assets/js/datatables-buttons/js/buttons.bootstrap4.min.js','public/js/admin_asset/buttons.bootstrap4.min.js')
.scripts('resources/admin_assets/js/jszip/jszip.min.js','public/js/admin_asset/jszip.min.js')
.scripts('resources/admin_assets/js/pdfmake/pdfmake.min.js','public/js/admin_asset/pdfmake.min.js')
.scripts('resources/admin_assets/js/pdfmake/vfs_fonts.js','public/js/admin_asset/vfs_fonts.js')
.scripts('resources/admin_assets/js/datatables-buttons/js/buttons.html5.min.js','public/js/admin_asset/buttons.html5.min.js')
.scripts('resources/admin_assets/js/datatables-buttons/js/buttons.print.min.js','public/js/admin_asset/buttons.print.min.js')
.scripts('resources/admin_assets/js/datatables-buttons/js/buttons.colVis.min.js','public/js/admin_asset/buttons.colVis.min.js')
.scripts('resources/admin_assets/js/sweetalert2/sweetalert2.min.js','public/js/admin_asset/sweetalert2.min.js')
.scripts('resources/admin_assets/js/toastr/toastr.min.js','public/js/admin_asset/toastr.min.js')
.scripts('resources/admin_assets/js/blockui/blockui.min.js','public/js/admin_asset/blockui.min.js')
.scripts('resources/admin_assets/js/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js','public/js/admin_asset/jquery.bootstrap-duallistbox.min.js')
.scripts('resources/admin_assets/js/select2/js/select2.full.min.js','public/js/admin_asset/select2.full.min.js')
.sourceMaps();

mix.styles('resources/admin_assets/css/all.min.css','public/css/admin_asset/all.min.css')
.styles('resources/admin_assets/css/icheck-bootstrap.min.css','public/css/admin_asset/icheck-bootstrap.min.css')
.styles('resources/admin_assets/css/adminlte.css','public/css/admin_asset/adminlte.css')
.styles('resources/admin_assets/css/fontawesome.min.css','public/css/admin_asset/fontawesome.min.css')
.styles('resources/admin_assets/dist/css/adminlte.min.css','public/css/admin_asset/adminlte.min.css')
.styles('resources/admin_assets/js/overlayScrollbars/css/OverlayScrollbars.min.css','public/css/admin_asset/OverlayScrollbars.min.css')
.styles('resources/admin_assets/js/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css','public/css/admin_asset/tempusdominus-bootstrap-4.min.css')
.styles('resources/admin_assets/js/daterangepicker/daterangepicker.css','public/css/admin_asset/daterangepicker.css')
.styles('resources/admin_assets/js/dropzone/min/dropzone.min.css','public/css/admin_asset/dropzone.min.css')
.styles('resources/admin_assets/js/datatables-bs4/css/dataTables.bootstrap4.min.css','public/css/admin_asset/dataTables.min.css')
.styles('resources/admin_assets/js/datatables-responsive/css/responsive.bootstrap4.min.css','public/css/admin_asset/responsive.bootstrap4.min.css')
.styles('resources/admin_assets/js/datatables-buttons/css/buttons.bootstrap4.min.css','public/css/admin_asset/buttons.bootstrap4.min.css')
.styles('resources/admin_assets/js/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css','public/css/admin_asset/sweetalert2.min.css')
.styles('resources/admin_assets/js/toastr/toastr.min.css','public/css/admin_asset/toastr.min.css')
.styles('resources/admin_assets/js/select2-bootstrap4-theme/select2-bootstrap4.min.css','public/css/admin_asset/select2-bootstrap4.min.css')
.styles('resources/admin_assets/js/select2/css/select2.min.css','public/css/admin_asset/select2.min.css')

.copyDirectory('resources/admin_assets/webfonts','public/css/webfonts')
.copyDirectory('resources/admin_assets/dist/img','public/admin_asset/img')
mix.webpackConfig({
    plugins: [
        new webpack.ProvidePlugin({
            'window.Swal': 'sweetalert2/dist/sweetalert2.all.min.js'
        })
    ]
});
