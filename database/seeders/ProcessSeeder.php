<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\MasterProcess;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class ProcessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        try {
            $process = [
                [
                    'description'   => 'embro',
                    'price'         => '15000',
                    'created_by'    => '1'
                ],
                [
                    'description'   => 'printing',
                    'price'         => '45000',
                    'created_by'    => '1'
                ]
            ];

            MasterProcess::insert($process);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
