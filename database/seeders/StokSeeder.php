<?php

namespace Database\Seeders;

use App\Models\BahanBaku;
use App\Models\StockBahan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StokSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();
            
            $data = BahanBaku::all();
            $dataToInsert = [];

            foreach ($data as $val) {
                switch ($val->bahan_type) {
                    case 'benang':
                        $supplier = 3;
                        break;
                    case 'cat':
                        $supplier = 2;
                        break;
                    case 'mica':
                        $supplier = 4;
                        break;
                    case 'alcohol':
                        $supplier = 5;
                        break;
                    default:
                        $supplier = null;
                        break;
                }

                $qty = mt_rand(10, 100);
                $dataToInsert[] = [
                    'bahan_id' => $val->id,
                    'qty' => $qty,
                    'qty_used' => 0,
                    'qty_available' => $qty,
                    'price' => $qty * ($val->unit_prices ?? 0),
                    'pemasok' => $supplier,
                    'created_by' => 1,
                    'created_at' => now(),
                    'uom' => $val->uom
                ];
            }
            DB::table('transaction_stock_bahan')->insert($dataToInsert);
            DB::commit();
            info('Data Successfully Created!');
        } catch (\Throwable $th) {
            DB::rollback();
            dd($th->getMessage());
        }
    }

}
