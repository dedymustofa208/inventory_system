<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\MasterSize;
use Illuminate\Support\Facades\DB;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        try {
            $size = [
                [
                    'description'   => 's',
                    'additional_price'  => '0'
                ],
                [
                    'description'   => 'm',
                    'additional_price'  => '0'
                ],
                [
                    'description'   => 'l',
                    'additional_price'  => '0'
                ],
                [
                    'description'   => 'xl',
                    'additional_price'  => '1000'
                ],
                [
                    'description'   => 'xxl',
                    'additional_price'  => '2000'
                ],
                [
                    'description'   => 'xxs',
                    'additional_price'  => '0'
                ],
                [
                    'description'   => 'xs',
                    'additional_price'  => '0'
                ],
                [
                    'description'   => 'youth',
                    'additional_price'  => '500'
                ],
            ];

            MasterSize::insert($size);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
