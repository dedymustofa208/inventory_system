<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\MasterBarang;
use Illuminate\Support\Facades\DB;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        try {
            $barang = [
                [
                    'description'   => 'jacket',
                    'price'         => '60000'
                ],
                [
                    'description'   => 'short pant',
                    'price'         => '35000'
                ],
                [
                    'description'   => 't-shirt',
                    'price'         => '15000'
                ],
                [
                    'description'   => 'tank top',
                    'price'         => '10000'
                ],
                [
                    'description'   => 'long pant',
                    'price'         => '50000'
                ],
                [
                    'description'   => 'sweater',
                    'price'         => '50000'
                ],
                [
                    'description'   => 'sweater shirt',
                    'price'         => '50000'
                ],
            ];

            MasterBarang::insert($barang);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
