<?php

namespace Database\Seeders;

use App\Models\BahanBaku;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'benang' => [
                'items' => [
                    'Cotton Thread', 'Polyester Thread', 'Nylon Thread', 'Silk Thread',
                    'Rayon Thread', 'Acrylic Thread', 'Linen Thread', 'Kevlar Thread',
                    'Wool Thread', 'Metallic Thread',
                ],
                'colors' => [
                    'hitam', 'putih', 'merah', 'biru', 'kuning', 'hijau', 'ungu', 'oranye',
                    'cokelat', 'abu_abu', 'pink', 'biru_tua', 'jingga', 'emas', 'perak',
                    'zaitun', 'turquoise', 'magenta', 'koral', 'teal', 'violet',
                ],
                'uoms' => 'm',
                'unit_prices' => mt_rand(35000, 95000)
            ],
            'cat' => [
                'items' => [
                    'Water-Based Ink', 'Solvent-Based Ink', 'UV Ink', 'Plastisol Ink',
                    'Discharge Ink', 'Sublimation Ink', 'Latex Ink', 'Pigment Ink',
                    'Reactive Ink', 'Acrylic Ink'
                ],
                'colors' => [
                    'hitam', 'putih', 'merah', 'biru', 'kuning', 'hijau', 'ungu', 'oranye',
                    'cokelat', 'abu_abu', 'pink', 'biru_tua', 'jingga', 'emas', 'perak',
                    'zaitun', 'turquoise', 'magenta', 'koral', 'teal', 'violet',
                ],
                'uoms' => 'ml',
                'unit_prices' => mt_rand(45000, 110000)
            ],
            'mica' => [
                'items' => [
                    'Screen 90T Polyester', 'Screen 120T Polyester', 'Screen 150T Polyester',
                    'Screen 180T Polyester', 'Screen 200T Polyester', 'Screen 230T Polyester',
                    'Screen 250T Stainless Steel', 'Screen 280T Stainless Steel',
                    'Screen 300T Stainless Steel', 'Screen 350T Stainless Steel'
                ],
                'colors' => ['green', 'white'],
                'uoms' => 'pcs',
                'unit_prices' => mt_rand(30000, 100000)
            ],
            'alcohol' => [
                'items' => [
                    'Isopropyl', 'Ethanol', 'Methanol', 'Butanol', 'Denatured'
                ],
                'colors' => ['non_color'],
                'uoms' => 'ml',
                'unit_prices' => mt_rand(25000, 65000)
            ]
        ];

        try {
            DB::beginTransaction();

            foreach ($data as $bahanType => $bahan) {
                foreach ($bahan['items'] as $item) {
                    foreach ($bahan['colors'] as $color) {
                        BahanBaku::create([
                            'description' => Str::of($item)->trim()->lower(),
                            'code_bahan' => $this->genCode(),
                            'color_name' => $color,
                            'bahan_type' => $bahanType,
                            'uom' => $bahan['uoms'],
                            'unit_prices' => $bahan['unit_prices'],
                            'created_by' => 1,
                            'created_at' => now()
                        ]);
                    }
                }
            }
            DB::commit();
            info('Successfully Created Bahan');
        } catch (\Throwable $th) {
            DB::rollback();
            dd($th->getMessage());
        }
    }

    private function genCode()
    {
        do {
            $kode = Str::upper(Str::random(6)); // Panjang kode diperpendek jadi 6
        } while (BahanBaku::where('code_bahan', $kode)->exists());

        return $kode;
    }
}
