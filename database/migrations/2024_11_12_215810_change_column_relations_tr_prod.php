<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnRelationsTrProd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_production', function (Blueprint $table) {
            $table->unsignedBigInteger('created_by')->nullable(false)->change();
            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('stock_id')->nullable(false)->change();
            $table->foreign('stock_id')
                ->references('id')
                ->on('transaction_stock_bahan')
                ->onDelete('cascade');
            // $table->string('invoice_number')->nullable(false)->change();
            // $table->foreign('invoice_number')
            // ->references('invoice_number')
            // ->on('transaction_order')
            // ->onUpdate('cascade')
            // ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_production', function (Blueprint $table) {
            $table->dropForeign('created_by');
            $table->dropForeign('stock_id');
            // $table->dropForeign('invoice_number');
        });
    }
}
