<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnRelationsMstProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_product', function (Blueprint $table) {
            $table->unsignedBigInteger('created_by')->nullable(false)->change();
            $table->foreign('created_by')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
            $table->unsignedBigInteger('barang_id')->nullable(false)->change();
            $table->foreign('barang_id')
            ->references('id')
            ->on('master_barang')
            ->onDelete('cascade');
            $table->unsignedBigInteger('process_id')->nullable(false)->change();
            $table->foreign('process_id')
            ->references('id')
            ->on('master_process')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_product', function (Blueprint $table) {
            $table->dropForeign(['created_by','deleted_by','updated_by','barang_id','process_id']);
        });
    }
}
