<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_bahan_baku', function (Blueprint $table) {
            $table->decimal('unit_prices', $precision=18, $scale=2)->default(0);
            $table->string('uom')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_bahan_baku', function (Blueprint $table) {
            $table->dropColumn('unit_prices');
            $table->dropColumn('uom');
        });
    }
}
