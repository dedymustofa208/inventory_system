<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_order_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->references('id')->on('transaction_order')->onDelete('cascade');
            $table->integer('qty')->default(0);
            $table->decimal('unit_price', $precision = 18, $scale = 2)->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_order_details');
    }
}
