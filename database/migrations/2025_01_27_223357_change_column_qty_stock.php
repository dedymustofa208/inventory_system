<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnQtyStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_stock_bahan', function (Blueprint $table) {
            $table->decimal('qty', $precision=18, $scale=2)->change();
            $table->decimal('qty_used', $precision=18, $scale=2)->change();
            $table->decimal('qty_available', $precision=18, $scale=2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_stock_bahan', function (Blueprint $table) {
            $table->bigInteger('qty')->default(0)->change();
            $table->bigInteger('qty_used')->default(0)->change();
            $table->bigInteger('qty_available')->default(0)->change();
        });
    }
}
