<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovementStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movement_stock', function (Blueprint $table) {
            $table->id();
            $table->integer('stock_id')->nullable();
            $table->string('invoice_number')->nullable();
            $table->float('qty')->nullable();
            $table->string('uom')->nullable();
            $table->string('description')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movement_stock');
    }
}
