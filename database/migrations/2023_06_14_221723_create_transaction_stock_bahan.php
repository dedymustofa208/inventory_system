<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionStockBahan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_stock_bahan', function (Blueprint $table) {
            $table->id();
            $table->integer('bahan_id')->nullable();
            $table->biginteger('qty')->nullable();
            $table->biginteger('qty_used')->nullable();
            $table->biginteger('qty_available')->nullable();
            $table->biginteger('price')->nullable();
            $table->string('pemasok')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_stock_bahan');
    }
}
