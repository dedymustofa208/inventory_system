<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewOrderDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW v_order_detail_rev AS SELECT 
        tos.client_order,
        tos.invoice_number,
        tod.qty,
        mb.description as nama_barang,
        tod.product_id,
        mp.size,
        mpp.description as process_name,
        tod.unit_price,
        tos.is_completed,
        tos.status,
        tos.created_at as order_date
        FROM transaction_order tos
        JOIN transaction_order_details tod
        JOIN master_product mp ON FIND_IN_SET(mp.id, tod.product_id)
        JOIN master_barang mb on mb.id = mp.barang_id
        LEFT JOIN master_process mpp on mpp.id = mp.process_id
        WHERE tos.deleted_at IS NULL
        ORDER BY tos.created_at ASC,mb.description");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS v_order_detail_rev");
    }
}
