<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVStockBahan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW v_stock_bahan AS SELECT 
        tsb.id,
        mbb.description as nama_bahan,
        mbb.bahan_type,
        mbb.code_bahan,
        tsb.uom,
        tsb.qty_used,
        tsb.qty_available,
        GROUP_CONCAT(DISTINCT ms.invoice_number SEPARATOR ',') AS invoice_number,
        tsb.created_at as tanggal_beli,
        max(ms.last_used) as last_used
        FROM transaction_stock_bahan tsb
        LEFT JOIN master_bahan_baku mbb on mbb.id = tsb.bahan_id
        LEFT JOIN(
        SELECT invoice_number,stock_id,max(created_at) as last_used from movement_stock GROUP BY invoice_number,stock_id
        )ms on ms.stock_id = tsb.id
        GROUP BY
        mbb.bahan_type,
        mbb.description,
        tsb.id,
        mbb.code_bahan,
        tsb.uom,
        tsb.qty_used,
        tsb.qty_available,
        tsb.created_at");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_stock_bahan');
    }
}
