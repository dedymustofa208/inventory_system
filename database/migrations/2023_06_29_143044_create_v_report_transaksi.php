<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVReportTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW v_report_transaksi AS SELECT 
        tos.client_order,
        tos.invoice_number,
        tos.qty,
        mb.description as nama_barang,
        mp.size,
        mpp.description as process_name,
        tos.total_price,
        tos.is_completed,
        tos.status,
        tos.created_at as order_date,
        tos.deleted_at as cancel_date,
        u.name as created_by,
        u2.name as cancel_by,
        CASE WHEN tos.is_completed = TRUE THEN MAX(tpp.updated_at) ELSE NULL end as finish_date
        FROM transaction_order tos
        JOIN master_product mp ON FIND_IN_SET(mp.id, tos.product_id)
        JOIN master_barang mb on mb.id = mp.barang_id
        JOIN transaction_production tpp on tpp.invoice_number = tos.invoice_number
        JOIN users u on u.id = tos.created_by
        LEFT JOIN users u2 on u2.id = tos.deleted_by
        LEFT JOIN master_process mpp on mpp.id = mp.process_id
        GROUP BY 
        tos.created_at,
        mb.description,
        tos.client_order,
        tos.invoice_number,
        tos.qty,
        mp.size,
        tos.total_price,
        tos.is_completed,
        mpp.description,
        tos.status,
        tos.deleted_at,
        u.name,
        u2.name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_report_transaksi');
    }
}
